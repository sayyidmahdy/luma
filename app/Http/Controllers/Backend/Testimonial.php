<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Helper;
use Illuminate\Support\Facades\Input;
use DB, Session, DataTables;

class Testimonial extends Controller
{
    public function __construct(Request $request, Helper $helper)
    {
        $this->request = $request;
        $this->helper = $helper;
    }

    public function index(){
        $breadcrumb = $this->helper->breadcrumb();
        
        return view('backend.testimonial.list', compact('breadcrumb'));
    }

    public function form($act, $id = null){
        $data['act'] = $act;
        $data['breadcrumb'] = $this->helper->breadcrumb();

        if ($act == 'edit') {
            $data['data'] = DB::table('ts_testimonial')->where('id_testimoni', $id)->first();
        }
        
        return view('backend.testimonial.form', $data);
    }

    public function submit($act, $id = ''){
        $file = $this->request->file('file');
        $name = $this->request->input('name');
        $description = $this->request->input('description');

        switch ($act) {
            case 'add':
                if ($file) {
                    $fileExtension = $file->getClientOriginalExtension();
                    $fileName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                    $path = public_path() . '/upload/testimonial/';
                    $date = date("YmdHis");
                    $newFileName = $fileName . '-' . $date . '.' . $fileExtension;
                }

                $data = [
                    // 'image' => $newFileName,
                    'name' => $name,
                    'description' => $description,
                ];

                $check = DB::table('ts_testimonial')->where('is_publish', 'Y')->count();
                ($check <= 3) ? $data['is_publish'] = 'Y' : $data['is_publish'] = 'N';

                $exec = DB::table('ts_testimonial')->insert($data);
                if ($exec) {
                    // $file->move($path, $newFileName);
                    $msg = 'success|Testimonial inserted succesfully';
                }
                else {
                    $msg = 'success|Testimonial inserted fail';
                }

                break;
            case 'edit':
                $data = [
                    'name' => $name,
                    'description' => $description,
                ];

                $exec = DB::table('ts_testimonial')->where('id_testimoni', $id)->update($data);

                if ($exec) {
                    $msg = 'success|Testimonial has been updated';
                }
                else {
                    $msg = 'error|Testimonial updated fail';
                }

                break;
            
            default:
                $exec = DB::table('ts_testimonial')->where('id_testimoni', $id)->delete();
                if ($exec) {
                    $msg = 'success|Testimonial has been deleted';
                }
                else {
                    $msg = 'error|Testimonial deleted fail';
                }

                break;
        }

        return $msg;
    }

    public function datatables(){
        DB::statement(DB::raw('set @rownum=0'));
        $data = DB::table('ts_testimonial')->selectraw('*, @rownum  := @rownum  + 1 AS rownum')->get();
        return DataTables::of($data)->make(true);
    }

    public function option($type, $id, $value){
        if ($type == 'status') {
            $data['status'] = ($value == 1) ? 2 : 1;
            $msg = 'success|change status succesfully';
        }
        else {
            $data['is_publish'] = ($value == 'Y') ? 'N' : 'Y';
            $check = DB::table('ts_testimonial')->where('is_publish', 'Y')->count();

            if($check == 3 && $value = 'N') {
                $msg = 'error|data displayed cannot be more than 3';
                return $msg;
            } 
            else {
                $msg = 'success|data published succesfully';
            }

        }

        DB::table('ts_testimonial')->where('id_testimoni', $id)->update($data);

        return $msg;
    }
}
