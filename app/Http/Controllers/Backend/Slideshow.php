<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Helper;
use Illuminate\Support\Facades\Input;
use DB, DataTables;

class Slideshow extends Controller
{
	public function __construct(Request $request, Helper $helper)
	{
	    $this->request = $request;
	    $this->helper = $helper;
	}

    public function index(){
    	$breadcrumb = $this->helper->breadcrumb();
    	
    	return view('backend.slideshow.list', compact('breadcrumb'));
    }

    public function form($act, $id = null){
    	$breadcrumb = $this->helper->breadcrumb();
        
        if ($act == 'edit') {
            $data = DB::table('ts_slideshow')->where('id_slideshow', $id)->first();
        	return view('backend.slideshow.form', compact('act', 'breadcrumb', 'data', 'id'));
        }
        else {
            return view('backend.slideshow.form', compact('act', 'breadcrumb'));
        }
    }

    public function submit($act, $id = NULL){
        switch ($act) {
            case 'add':
                $type = $this->request->input('type');
                $url = $this->request->input('url');
                $file = $this->request->file('file');

                $fileExtension = $file->getClientOriginalExtension();
                $fileName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $path = public_path() . '/upload/slideshow/';
                $date = date("YmdHis");
                $newFileName = $fileName . '-' . $date . '.' . $fileExtension;
                $data = [
                    'file' => $newFileName,
                    'url' => $url,
                    'type' => $type,
                ];

                $exec = DB::table('ts_slideshow')->insert($data);
                if ($exec) {
                    $file->move($path, $newFileName);

                    $msg = 'success|Slideshow uploaded succesfully';
                }
                else {
                    $msg = 'error|Slideshow uploaded fail';
                }

                break;

            case 'edit':
                $file = $this->request->file('file');
                if ($file) {
                    $fileExtension = $file->getClientOriginalExtension();
                    $fileName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                    $path = public_path() . '/upload/slideshow/';
                    $date = date("YmdHis");
                    $newFileName = $fileName . '-' . $date . '.' . $fileExtension;
                    $data['file'] = $newFileName;
                }

                $url = $this->request->input('url');
                $type = $this->request->input('type');

                $data = [
                    'url' => ($type == 'video') ? $url : NULL,
                    'type' => $type,
                ];

                $exec = DB::table('ts_slideshow')->where('id_slideshow', $id)->update($data);
                if ($exec) {

                    if ($file) {
                        $file->move($path, $newFileName);
                    }

                    $msg = 'success|Slideshow has been updated';
                }
                else {
                    $msg = 'error|Slideshow updated fail';
                }

                break;
            
            default:
                $exec = DB::table('ts_slideshow')->where('id_slideshow', $id)->delete();

                $msg = ($exec) ? 'success|Slideshow has been deleted' : 'error|Slideshow deleted failed';
                break;
        }
    	

        return $msg;
    }

    public function datatables(){
        DB::statement(DB::raw('set @rownum=0'));
        $data = DB::table('ts_slideshow')->selectraw('*, @rownum  := @rownum  + 1 AS rownum')->get();
        return DataTables::of($data)->make(true);
    }

    
}
