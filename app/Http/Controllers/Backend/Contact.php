<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Helper;
use Illuminate\Support\Facades\Input;
use DB, Session, DataTables;

class Contact extends Controller
{
    public function __construct(Request $request, Helper $helper)
    {
        $this->request = $request;
        $this->helper = $helper;
    }

    public function index(){
    	$breadcrumb = $this->helper->breadcrumb();
    	return view('backend.contact.list', compact('breadcrumb'));
    }

    public function datatables(){
        DB::statement(DB::raw('set @rownum=0'));
        $data = DB::table('ts_contact')->selectraw('*, @rownum  := @rownum  + 1 AS rownum')->get();
        return DataTables::of($data)->make(true);
    }

    public function deletes($id){
        $exec = DB::table('ts_contact')->where('id_contact', $id)->delete();

        $msg = ($exec) ? 'success|Contact has been deleted' : 'error|Contact deleted failed';
        return $msg;
    }
}
