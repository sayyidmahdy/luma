<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Helper;
use Illuminate\Support\Facades\Input;
use App\Model\{m_galery};
use DB, Session, DataTables, Image, File;
use Illuminate\Filesystem\Filesystem;

class Galery extends Controller
{
    public function __construct(Request $request, Helper $helper)
	{
	    $this->request = $request;
	    $this->helper = $helper;
	}

    public function index(){
    	$breadcrumb = $this->helper->breadcrumb();
    	
    	return view('backend.galery.list', compact('breadcrumb'));
    }

    public function form($act = null, $id = null){
    	$breadcrumb = $this->helper->breadcrumb();
        $type = DB::table('tm_type')->get();
        $sosmed = DB::table('tm_sosmed')->get();
    	
        if ($act == 'add') {
        	return view('backend.galery.form', compact('breadcrumb', 'sosmed', 'type'));
        }
        else {
            $data = DB::table('ts_galery_header')->where('id', $id)->first();
            return view('backend.galery.form-edit', compact('act', 'id', 'type', 'data'));
        }
    }

    public function form_add_item($id){
        $breadcrumb = $this->helper->breadcrumb();
        $type = DB::table('tm_type')->get();
        $sosmed = DB::table('tm_sosmed')->get();

        return view('backend.galery.form-item', compact('id', 'breadcrumb', 'sosmed', 'type'));
    }

    public function submit($act, $id = null){
        $fileHdr = $this->request->file('thumbnail');

        if ($fileHdr) {
            $fileExtension = $fileHdr->getClientOriginalExtension();
            $fileName      = pathinfo($fileHdr->getClientOriginalName(), PATHINFO_FILENAME);
            $date          = date("YmdHis");
            $newThumbnail  = $fileName . '-' . $date . '.' . $fileExtension;
        }

    	$data = $this->request->input('data');
        
        switch ($act) {
            case 'add':
                $video = $this->request->input('video');
                $fileDtl = $this->request->file('image');

                $exec_hdr = new m_galery;
                $exec_hdr->title = $data['title'];
                $exec_hdr->type_id = $data['type_id'];
                $exec_hdr->thumbnail = $newThumbnail;
                $exec_hdr->created_by = 1;
                $exec_hdr->save();

                if ($exec_hdr) {
                    $idHdr = $exec_hdr->id;
                    $path = public_path() . '/upload/galery/'.$idHdr.'/thumbnail/';
                    $fileHdr->move($path, $newThumbnail);

                    foreach ($fileDtl as $key => $value) 
                    {
                        $files = $value['file'];
                        $dtlExtension = $files->getClientOriginalExtension();
                        $dtlFileName  = pathinfo($files->getClientOriginalName(), PATHINFO_FILENAME);
                        $dtlDate      = date("YmdHis");
                        $newFileName  = $dtlFileName . '-' . $dtlDate . '.' . $dtlExtension; 
         
                        $imgDtl = [
                            'header_id' => $idHdr,
                            'file' => $newFileName,
                            'created_by' => 1
                        ];

                        $dtlPath = public_path() . '/upload/galery/'.$idHdr.'/detail/'; 
                        $files->move($dtlPath, $newFileName);
                        DB::table('ts_galery_detail')->insert($imgDtl);
                    }

                    if ($video[0]['vidio']) {
                        foreach ($video as $key => $value) {
                            $vidioDtl = [
                                'header_id' => $idHdr,
                                'source_url' => $value['vidio'],
                                'type_id' => $value['type'],
                                'created_by' => 1
                            ];

                            DB::table('ts_galery_detail')->insert($vidioDtl);
                        }
                    }

                }

                $msg = 'success|Galery inserted successfully';
                break;
            
            case 'edit':
                $exec = m_galery::find($id);
                $exec->title = $data['title'];
                $exec->type_id = $data['type_id'];

                if ($fileHdr) {
                    $exec->thumbnail = $newThumbnail;
                }

                $exec->save();
                if ($exec) {
                    if ($fileHdr) {
                        $path = public_path() . '/upload/galery/'.$id.'/thumbnail/';
                        $fileHdr->move($path, $newThumbnail);
                    }

                    $msg = 'success|Galery updated successfully';
                }
                else {
                    $msg = 'error|Galery updated fail';
                }

                break;

            default:
                $exec = m_galery::where('id', $id)->delete();
                if ($exec) {
                    DB::table('ts_galery_detail')->where('header_id', $id)->delete();
                    File::deleteDirectory(public_path('/upload/galery/'.$id));
                    $msg = 'success|Galery has been deleted';
                }
                else {
                    $msg = 'error|Galery delete fail';
                }

                return $msg;
                break;
        }

    	Session::flash('message', $msg);
        return redirect('backend/galery');
    }

    public function add_item($id){
        $video = $this->request->input('video');
        $fileDtl = $this->request->file('image');

        foreach ($fileDtl as $key => $value) 
        {
            $files = $value['file'];
            $dtlExtension = $files->getClientOriginalExtension();
            $dtlFileName  = pathinfo($files->getClientOriginalName(), PATHINFO_FILENAME);
            $dtlDate      = date("YmdHis");
            $newFileName  = $dtlFileName . '-' . $dtlDate . '.' . $dtlExtension; 

            $imgDtl = [
                'header_id' => $id,
                'file' => $newFileName,
                'created_by' => 1
            ];

            $dtlPath = public_path() . '/upload/galery/'.$id.'/detail/'; 
            $files->move($dtlPath, $newFileName);
            DB::table('ts_galery_detail')->insert($imgDtl);
        }

        if ($video[0]['vidio']) {
            foreach ($video as $key => $value) {
                $vidioDtl = [
                    'header_id' => $id,
                    'source_url' => $value['vidio'],
                    'type_id' => $value['type'],
                    'created_by' => 1
                ];

                DB::table('ts_galery_detail')->insert($vidioDtl);
            }
        }

        Session::flash('message', 'success|Galery inserted successfully');
        return redirect('backend/galery');
    }

    public function datatables(){
        DB::statement(DB::raw('set @rownum=0'));
        $data = DB::table('ts_galery_header as a')
                ->join('tm_type as b', 'a.type_id', 'b.id_type')
                ->selectraw('@rownum  := @rownum  + 1 AS rownum, a.*, b.name as type_name, (select count(*) from ts_galery_detail where header_id = a.id and file is not null) as total_foto, (select count(*) from ts_galery_detail where header_id = a.id and file is null) as total_video')
                ->get();

        return DataTables::of($data)->make(true);
    }

    public function resizeImage($file, $fileNameToStore) {
        // Resize image
        $resize = Image::make($file)->resize(600, null, function ($constraint) {
            $constraint->aspectRatio();
        })->encode('jpg');

        // Create hash value
        $hash = md5($resize->__toString());

        // Prepare qualified image name
        $image = $hash."jpg";

        // Put image to storage
        $save = Storage::put("public/images/{$fileNameToStore}", $resize->__toString());

        if($save) {
            return true;
        }
        return false;
    }
}
