<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Helper;
use Illuminate\Support\Facades\Input;
use DB, Session, DataTables, Image, File;

class About extends Controller
{
	public function __construct(Request $request, Helper $helper)
	{
	    $this->request = $request;
	    $this->helper = $helper;
	}

    public function index(){
    	$breadcrumb = $this->helper->breadcrumb();
    	
    	return view('backend.about.list', compact('breadcrumb'));
    }

     public function form($act, $id = null){
        $data['act'] = $act;
        $data['breadcrumb'] = $this->helper->breadcrumb();

        if ($act == 'edit') {
            $data['data'] = DB::table('ts_about_us')->where('id', $id)->first();
        }
        
        return view('backend.about.form', $data);
    }

    public function submit($act, $id = ''){
        $file = $this->request->file('file');
        $name = $this->request->input('name');
        $position = $this->request->input('position');
        $facebook = $this->request->input('facebook');
        $instagram = $this->request->input('instagram');
        $twitter = $this->request->input('twitter');
        $data = [
            'name' => $name,
            'position' => $position,
            'facebook' => $facebook,
            'instagram' => $instagram,
            'twitter' => $twitter,
        ];

        if ($file) {
            $fileExtension = $file->getClientOriginalExtension();
            $fileName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $path = public_path() . '/upload/about/';
            $date = date("YmdHis");
            $newFileName = $fileName . '-' . $date . '.' . $fileExtension;
            $data['image'] = $newFileName;
        }

        switch ($act) {
        	case 'add':
        		$exec = DB::table('ts_about_us')->insert($data);
        		if ($exec) {
		        	if ($file) {
			            $file->move($path, $newFileName);
		        	}

		            $msg = 'success|About inserted succesfully';
		        }
		        else {
		            $msg = 'success|About inserted fail';
		        }

        		break;

    		case 'edit':
        		$exec = DB::table('ts_about_us')->where('id', $id)->update($data);

		        if ($exec) {
		        	if ($file) {
			            $file->move($path, $newFileName);
		        	}

		            $msg = 'success|About has been updated';
		        }
		        else {
		            $msg = 'success|About updated fail';
		        }
        
        		break;
        	
        	default:
        		$exec = DB::table('ts_about_us')->where('id', $id)->delete();
        		if ($exec) {
		        	if ($file) {
			            $file->move($path, $newFileName);
		        	}

		            $msg = 'success|About has been deleted';
		        }
		        else {
		            $msg = 'success|About deleted fail';
		        }

        		break;
        }

        return $msg;
    }

    public function datatables(){
        DB::statement(DB::raw('set @rownum=0'));
        $data = DB::table('ts_about_us as a')
                ->selectraw('@rownum  := @rownum  + 1 AS rownum, a.*')
                ->get();

        return DataTables::of($data)->make(true);
    }
}
