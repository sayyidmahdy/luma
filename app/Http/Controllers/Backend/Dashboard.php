<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Helper;
use Illuminate\Support\Facades\Input;
use DB, Session, Auth;
use Analytics;
use Spatie\Analytics\Period;

class Dashboard extends Controller
{
	public function __construct(Request $request, Helper $helper)
	{
	    $this->request = $request;
	    $this->helper = $helper;
	}

    public function index(){
    	$breadcrumb = $this->helper->breadcrumb();
    	return view('backend.dashboard.dashboard', compact('breadcrumb'));
    }

    public function chart(){
        $fetch_browser = Analytics::fetchTopBrowsers(Period::days(365));
        $visitor = Analytics::fetchTotalVisitorsAndPageViews(Period::days(60));
        $browser = $fetch_browser->toArray();

        foreach ($browser as $k=> $v)
        {
            $color =[
                0 => 'success',
                1 => 'warning',
                2 => 'danger',
                3 => 'info',
                4 => 'primary',
                5 => 'secondary',
                6 => 'brand',
                7 => 'accent',
                8 => 'focus',
                9 => 'metal',
                10 => 'light',
            ];

            $browser[$k] ['name'] = $browser[$k] ['browser'];
            unset($browser[$k]['browser']); 

            $browser[$k] ['value'] = $browser[$k] ['sessions'];
            unset($browser[$k]['sessions']); 

            $browser[$k]['color'] = $color[$k];
        }

        return \Response::JSON([
            'pie_chart' => $browser,
            'visitor' => $visitor

        ]);
    }
}
