<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class Helper extends Controller
{
    public function breadcrumb(){
    	$url = $_SERVER['REQUEST_URI'];
    	$replace = str_replace('/backend/', '', $url);
    	$breadcrumb = explode('/', $replace);

    	return $breadcrumb;
    }

    public function menu_galery(){
    	$data = DB::table('tm_type')->get();
    	return response()->json($data);
    }
}
