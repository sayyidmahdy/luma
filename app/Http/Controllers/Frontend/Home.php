<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Helper;
use DB, Validator, Cookie, Session, Redirect, Mail, Hash;

class Home extends Controller
{
    public function __construct(Request $request, Helper $helper)
    {
        $this->request = $request;
        $this->helper = $helper;
    }

    public function index(){
    	$data['galery'] = DB::table('ts_galery_header as a')
                ->join('tm_type as b', 'a.type_id', 'b.id_type')
                ->selectraw('a.*, b.name as type_name')
                ->get();


        $data['testimoni'] = DB::table('ts_testimonial')->where('is_publish', 'Y')->where('status', 1)->limit('3')->get();
        $data['slideshow'] = DB::table('ts_slideshow')->get();

        $data['comment'] = DB::table('ts_comment')
                            ->whereNull('header_id')
                            ->where('is_reply', 'N')
                            ->selectraw("*, DATE_FORMAT(created_at, '%M %dth, %Y') AS new_date")
                            ->limit('5')
                            ->get();

        $data['reply'] = DB::table('ts_comment')
                            ->whereNull('header_id')
                            ->whereraw('is_reply <> "N"')
                            ->selectraw("*, DATE_FORMAT(created_at, '%M %dth, %Y') AS new_date")
                            ->get();

    	return view('frontend.home.home', $data);
    }

    public function contact(){
    	return view('frontend.home.contact');
    }

    public function about_us(){
        $data['data'] = DB::table('ts_about_us')->get();
        return view('frontend.home.about-us', $data);
    }

    public function login(){
    	return view('frontend.user.login');
    }

    public function do_login()
    {
        
        $rules = [
            'username' => 'required',
            'password' => 'required|min:6',
        ];

        $input = $this->request->all();

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $username = $this->request->input('username');
        $password = $this->request->input('password');
        $hashpassword = Hash::make($password);
        $user = DB::table('users')->where('username', $username)->first();

        if (!$user) {
            Session::flash('message', 'error|Login gagal. User tidak terdaftar.');
            return Redirect::back();
        }

        $hashedPassword = $user->password;

        $arrSession = [
            'username' => $user->username,
            'name'     => $user->name,
            'email'     => $user->email
        ];

        if (Hash::check($password, $hashedPassword)) {
            session($arrSession);
            Auth::loginUsingId($user->id);
            return Redirect::to('/backend/dashboard');
        } else {
            Session::flash('message', 'error|Login gagal. Password Anda salah.');
            return Redirect::back();
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }

    public function contact_submit(){
        $data = $this->request->all();
        
        $arrData = [
            'name' => $data['name'],
            'email' => $data['email'],
            'telephone' => $data['telephone'],
            // 'dates' => date_format(date_create($data['dates']), "Y-m-d"),
            'venue' => $data['venue'],
            'message' => $data['message']
        ];
        
        DB::table('ts_contact')->insert($arrData);
        return 'success';
    }

    public function template2(){
        return view('template.frontend.index2');
    }
}
