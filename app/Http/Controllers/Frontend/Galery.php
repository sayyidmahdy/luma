<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Helper;
use DB, Redirect, Session;

class Galery extends Controller
{
    public function __construct(Request $request, Helper $helper)
    {
        $this->request = $request;
        $this->helper = $helper;
    }

    public function index($category){
        $arrType = [];
        $dataType = DB::table('tm_type')->get();

        foreach ($dataType as $row) {
            $arrType[] = $row->name;
        }

        if (!in_array($category, $arrType)) {
            abort(404);
        }

        $idType = DB::table('tm_type')->where('name', $category)->value('id_type');
        $data['category'] = $category;
        $data['galery'] = DB::table('ts_galery_header as a')
                        ->join('tm_type as b', 'a.type_id', 'b.id_type')
                        ->where('type_id', $idType)
                        ->selectraw('a.*, b.name as type_name')
                        ->get();

    	if($category == 'Video')
        {
            $data['video'] = DB::table('ts_video AS a')->leftjoin('tm_type AS b', 'b.id_type', 'a.type')->get();
	    	return view('frontend.galery.video', $data);
        }
	    else
        {
	    	return view('frontend.galery.galery', $data);
        }
    }

    public function detail($category, $slug){
        $data['data'] = DB::table('ts_galery_header')->where('slug', $slug)->first();

        $idHdr = $data['data']->id;
        $data['header_id'] = $idHdr;

        $data['image'] = DB::table('ts_galery_detail as a')
                        ->leftjoin('tm_sosmed as b', 'a.type_id', 'b.id_sosmed')
                        ->where('header_id', $idHdr)
                        ->whereraw('file is not null')
                        ->get();

        $data['video'] = DB::table('ts_galery_detail as a')
                        ->leftjoin('tm_sosmed as b', 'a.type_id', 'b.id_sosmed')
                        ->where('header_id', $idHdr)
                        ->whereraw('file is null')
                        ->selectraw('a.*,  case when a.type_id = 1 then CONCAT(url, source_url) else CONCAT(url, source_url, "/embed") end as embed_url')
                        ->get();

        $data['comment'] = DB::table('ts_comment')
                            ->where('header_id', $idHdr)
                            ->where('is_reply', 'N')
                            ->selectraw("*, DATE_FORMAT(created_at, '%M %dth, %Y') AS new_date")
                            ->get();

        $data['reply'] = DB::table('ts_comment')
                            ->where('header_id', $idHdr)
                            ->whereraw('is_reply <> "N"')
                            ->selectraw("*, DATE_FORMAT(created_at, '%M %dth, %Y') AS new_date")
                            ->get();
                            
	    return view('frontend.galery.detail', $data);
    }

    public function comment_submit(){
        $data = $this->request->all();
        $data = $data['data'];
        
        DB::table('ts_comment')->insert($data);

        $message = 'success|Comment has been recorded.';
        Session::flash('message', $message);
        return redirect::back();
    }
}
