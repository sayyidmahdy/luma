<?php 

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('menu-galery', 'Helper@menu_galery');

Route::middleware('auth')->namespace('Backend')->prefix('backend')->group(function () { 
	Route::get('/dashboard', 'Dashboard@index');
	Route::get('/chart', 'Dashboard@chart');

	Route::prefix('slideshow')->group(function(){
		Route::get('/', 'Slideshow@index');
		Route::get('/form/{act}/{id?}', 'Slideshow@form');
		Route::post('/submit/{act}/{id?}', 'Slideshow@submit');
		Route::get('/datatables', 'Slideshow@datatables');
	});

	Route::prefix('galery')->group(function(){
		Route::get('/', 'Galery@index');
		Route::get('/form/{act}/{id?}', 'Galery@form');
		Route::post('/submit/{act}/{id?}', 'Galery@submit');\

		Route::get('/add-item/{id}', 'Galery@form_add_item');
		Route::post('/submit-item/{id}', 'Galery@add_item');
		Route::get('/datatables', 'Galery@datatables');
	});

	Route::prefix('testimonial')->group(function(){
		Route::get('/', 'Testimonial@index');
		Route::get('/form/{act}/{id?}', 'Testimonial@form');
		Route::post('/submit/{act}/{id?}', 'Testimonial@submit');
		Route::get('/option/{type}/{id}/{value}', 'Testimonial@option');
		Route::get('/datatables', 'Testimonial@datatables');
	});

	Route::prefix('contact')->group(function(){
		Route::get('/', 'Contact@index');
		Route::get('/option/{type}/{id}/{value}', 'Contact@option');
		Route::get('/datatables', 'Contact@datatables');
		Route::post('/delete/{id}', 'Contact@deletes');
	});

	Route::prefix('about')->group(function(){
		Route::get('/', 'About@index');
		Route::get('/form/{act}/{id?}', 'About@form');
		Route::post('/submit/{act}/{id?}', 'About@submit');
		Route::get('/datatables', 'About@datatables');
	});

	Route::prefix('video')->group(function(){
		Route::get('/', 'Video@index');
		Route::get('/form/{act}/{id?}', 'Video@form');
		Route::post('/submit/{act}/{id?}', 'Video@submit');
		Route::get('/option/{type}/{id}/{value}', 'Video@option');
		Route::get('/datatables', 'Video@datatables');
	});
});

Route::namespace('Frontend')->group(function () { 
	Route::get('/', 'Home@index');
	Route::get('/template2', 'Home@template2');
	Route::get('/about-us', 'Home@about_us');
	Route::get('/contact-us', 'Home@contact');
	Route::get('/login', 'Home@login')->name('login');
	Route::post('/do-login', 'Home@do_login');
	Route::get('/logout', 'Home@logout');

	Route::post('/contact-submit', 'Home@contact_submit');
	Route::post('/comment-submit', 'Galery@comment_submit');

	// always put on the last line
	Route::get('/{category}', 'Galery@index');
	Route::get('/{category}/{slug}', 'Galery@detail');
});