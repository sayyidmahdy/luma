(function(){
    $.ajaxSetup({
        timeout: 60000,
        cache: false,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function(){
            // loading();
        },
        complete: function(){
            // clearLoading();
        },
        error: function (x, e) {
            console.log(x);
            var msg = '';
            if (x.status == 0) {
                msg = 'Request Aborted!';
            } else if (x.status == 404) {
                msg = 'Page Not Found!';
            } else if (x.status == 500) {
                msg = 'Internal Server Error!';
            } else if (e == 'parsererror') {
                msg = 'Error.\nParsing JSON Request failed!';
            } else if (e == 'timeout') {
                msg = 'Request Timeout!';
            } else if (x.status == 401) {
                msg = 'Authentication Timeout!';
                window.location = site_url + '/logout';
            } else if (x.status == 419) {
                msg = 'Token Mismatch, Authentication Timeout!';
                window.location = site_url + '/logout';
            } else {
                msg = 'Error tidak diketahui: \n' + x.responseText;
            }
            swal('', msg, 'error');
        }
    });
})();

function loading()
{
    $('.page-loader').show();
}

function clearLoading()
{
    $('.page-loader').delay(1000).fadeOut(500);
}

function inputLoading(elm)
{
    var loading = '<img id="input_loading" src="'+base_url+'/img/facebook.gif">';
    elm.closest('.form-group').next().find('select').hide();
    if(elm.closest('.form-group').next().find('div').length > 0)
        elm.closest('.form-group').next().find('div').append(loading);
    else
        elm.closest('.form-group').next().append(loading);
}

function clearInputLoading(elm)
{
    elm.closest('.form-group').next().find('#input_loading').remove();
    elm.closest('.form-group').next().find('select').show();
}

function loadingbar(id)
{
    var loading_img = '<img id="input_loading" src="'+base_url+'/img/facebook.gif">'
    $('#'+id).show().html(loading_img);
}

function closeLoadingbar(id)
{
    $('#'+id).hide();
}

// MODAL
function Modal(id, title, body_url, footer, size) {
    if(footer.length > 0)
        var modal_footer = '<div class="modal-footer"></div>';
    else
        var modal_footer = '';

    modal_dialog(id, '\
        <div class="modal-content">\
            <div class="modal-header">\
                <h5 class="modal-title"></h5>\
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">\
                    <span aria-hidden="true"><span class="fa fa-times-circle-o"></span></span>\
                </button>\
            </div>\
            <div class="modal-body" style="max-height: 70vh; overflow-y: auto;">\
                <div id="modal_loading">\
                    <center><img id="input_loading" src="'+base_url+'/img/facebook.gif"></center>\
                </div>\
                <div id="modal_body_content" style="display: none"></div>\
            </div>\
            '+modal_footer+'\
        </div>');
    
    ModalTitle(id, title);
    ModalBody(id, body_url);
    if(footer.length > 0) ModalFooter(id, footer);
    ModalSize(id, size);
}
function ModalClose(id)
{
    $('#'+id).modal('hide');
}
function modal_div(id, inner) {
    div = document.createElement("div");
    div.innerHTML = '<div id="' + id + '" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">' + inner + '</div>';
    document.body.appendChild(div);
}
function modal_dialog(id, inner) {
    $('#' + id).remove();
    modal_div(id, '<div class="modal-dialog" role="document">' + inner + '</div>');
    
    $('#' + id).modal({
        backdrop: 'static',
        keyboard: false
    });
}
function ModalTitle(id, title)
{   
    $('#'+id).find('.modal-title').html(title);
}
function ModalBody(id, body)
{
    $('#'+id).find('.modal-body').find('#modal_body_content').load(body,
        function () {
            $('#'+id).find('.modal-body').find('#modal_body_content').slideDown();
            $('#'+id).find('.modal-body').find('#modal_loading').hide();
        }
    );
}
function ModalFooter(id, footer)
{
    $('#'+id).find('.modal-footer').html(footer);
}
function ModalSize(id, size)
{
    $('#'+id).find('.modal-dialog').addClass(size);
}
// MODAL

function showFlashAlert(type='info', message='')
{
    $('#flash_msg').slideUp();
    $('#flash_msg').find("div[class*='alert-']").removeClass (function (index, css) {
       return (css.match (/(^|\s)alert\S+/g) || []).join(' ');
    });
    $('#flash_msg').find('.alert').addClass(alertType(type).class);
    $('#flash_msg').find('#alert-title').html(alertType(type).title);
    $('#flash_msg').find('#alert-message').html(message);
    $('#flash_msg').slideDown();
    setTimeout(function() {
        $("#flash_msg").slideUp()
    }, 10000);
}
function closeFlashAlert()
{
    $('#flash_msg').fadeOut('slow');
}
function alertType(type)
{
    switch(type) {
        case 'default':
            var alert = {class: 'alert-default', title: 'Info'};
            break;
        case 'primary':
            var alert = {class: 'alert-primary', title: 'Info'};
            break;
        case 'secondary':
            var alert = {class: 'alert-secondary', title: 'Info'};
            break;
        case 'info':
            var alert = {class: 'alert-info', title: 'Info'};
            break;
        case 'success':
            var alert = {class: 'alert-success', title: 'Success'};
            break;
        case 'error':
            var alert = {class: 'alert-danger', title: 'Error'};
            break;
        case 'warning':
            var alert = {class: 'alert-warning', title: 'Warning'};
            break;
        default:
            var alert = {class: 'alert-default', title: 'Info'};
    }

    return alert;
}


function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
function formatNumber(data)
{
    var result = data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    return result;
}
function ReplaceAll(Source, stringToFind, stringToReplace) {
    var temp = Source;
    var index = temp.indexOf(stringToFind);
    while (index != -1) {
        temp = temp.replace(stringToFind, stringToReplace);
        index = temp.indexOf(stringToFind);
    }
    return temp;
}
function formatDate(date, format)
{
    var oDate = new Date(date);
    if(format.toLowerCase() == 'd-m-y')
    {
        result = (oDate.getDate()).toString().padStart(2, '0')+"-"+(oDate.getMonth()+1).toString().padStart(2, '0')+"-"+oDate.getFullYear();
    }
    else if(format.toLowerCase() == 'y-m-d')
    {
        result = oDate.getFullYear()+"-"+(oDate.getMonth()+1).toString().padStart(2, '0')+"-"+(oDate.getDate()).toString().padStart(2, '0');
    }
    else if(format.toLowerCase() == 'm-d-Y')
    {
        result = (oDate.getMonth()+1).toString().padStart(2, '0')+"-"+(oDate.getDate()).toString().padStart(2, '0')+"-"+oDate.getFullYear();
    }
    else
    {
        result = (oDate.getDate()).toString().padStart(2, '0')+"-"+(oDate.getMonth()+1).toString().padStart(2, '0')+"-"+oDate.getFullYear();
    }
    
    return result;
}
function ThausandSeperator(hidden, value, digit) {
    var thausandSepCh = ",";
    var decimalSepCh = ".";
    var tempValue = "";
    var realValue = value + "";
    var devValue = "";
    if (digit == "")
        digit = 3;
    realValue = characterControl(realValue);
    var comma = realValue.indexOf(decimalSepCh);
    if (comma != -1) {
        tempValue = realValue.substr(0, comma);
        devValue = realValue.substr(comma);
        devValue = removeCharacter(devValue, thausandSepCh);
        devValue = removeCharacter(devValue, decimalSepCh);
        devValue = decimalSepCh + devValue;
        if (devValue.length > digit) {
            devValue = devValue.substr(0, digit + 1);
        }
    } else {
        tempValue = realValue;
    }
    tempValue = removeCharacter(tempValue, thausandSepCh);
    var result = "";
    var len = tempValue.length;
    while (len > 3) {
        result = thausandSepCh + tempValue.substr(len - 3, 3) + result;
        len -= 3;
    }
    result = tempValue.substr(0, len) + result;
    if (hidden != "") {
        $("#" + hidden).val(tempValue + devValue);
    }
    return result + devValue;
}

function isNumber(e) {
    var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
    var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode == 45));
    return ret;
}

function ajax_alert(res){
    var msg = res.split('|');

    setTimeout(function() {
        showFlashAlert(msg[0], msg[1]);
    }, 1000);
}