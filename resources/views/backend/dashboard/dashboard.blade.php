@extends('template.backend.index')
@section('content')
    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver m-container m-container--responsive m-container--xxl m-page__container">
        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <div class="m-content">
                <!--Begin::Main Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__body  m-portlet__body--no-padding">
                        <div class="row m-row--no-padding m-row--col-separator-xl">
                            <div class="col-xl-4">
                                <!--begin:: Widgets/Daily Sales-->
                                <div class="m-widget14">
                                    <div class="m-widget14__header m--margin-bottom-30">
                                        <h3 class="m-widget14__title">
                                        Daily Sales
                                        </h3>
                                        <span class="m-widget14__desc">
                                            Check out each collumn for more details
                                        </span>
                                    </div>
                                    <div class="m-widget14__chart" style="height:120px;">
                                        <canvas id="m_chart_daily_sales"></canvas>
                                    </div>
                                </div>
                                <!--end:: Widgets/Daily Sales-->
                            </div>
                            <div class="col-xl-4">
                                <!--begin:: Widgets/Profit Share-->
                                <div class="m-widget14">
                                    <div class="m-widget14__header">
                                        <h3 class="m-widget14__title">
                                            Browser Data
                                        </h3>
                                        <span class="m-widget14__desc">
                                            Most browser used by user
                                        </span>
                                    </div>
                                    <div class="row  align-items-center">
                                        <div class="col">
                                            <div id="chart_pelanggan" class="m-widget14__chart" style="height: 160px">
                                                <div class="m-widget14__stat">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="m-widget14__legends" id="label_chart">
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end:: Widgets/Profit Share-->
                            </div>
                            <div class="col-xl-4">
                                <!--begin:: Widgets/Support Stats-->
                                <div class="m-widget1">
                                    <div class="m-widget1__item">
                                        <div class="row m-row--no-padding align-items-center">
                                            <div class="col">
                                                <h3 class="m-widget1__title">
                                                IPO Margin
                                                </h3>
                                                <span class="m-widget1__desc">
                                                    Awerage IPO Margin
                                                </span>
                                            </div>
                                            <div class="col m--align-right">
                                                <span class="m-widget1__number m--font-focus">
                                                    +24%
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-widget1__item">
                                        <div class="row m-row--no-padding align-items-center">
                                            <div class="col">
                                                <h3 class="m-widget1__title">
                                                Payments
                                                </h3>
                                                <span class="m-widget1__desc">
                                                    Yearly Expenses
                                                </span>
                                            </div>
                                            <div class="col m--align-right">
                                                <span class="m-widget1__number m--font-accent">
                                                    +$560,800
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-widget1__item">
                                        <div class="row m-row--no-padding align-items-center">
                                            <div class="col">
                                                <h3 class="m-widget1__title">
                                                Logistics
                                                </h3>
                                                <span class="m-widget1__desc">
                                                    Regional Logistics
                                                </span>
                                            </div>
                                            <div class="col m--align-right">
                                                <span class="m-widget1__number m--font-info">
                                                    -10%
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end:: Widgets/Support Stats-->
                            </div>
                        </div>
                    </div>
                </div>
                <!--END::Main Portlet-->
                <div class="row">
                    <div class="col-xl-12">
                        <!--begin:: Widgets/Latest Updates-->
                        <div class="m-portlet m-portlet--full-height m-portlet--fit ">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <h3 class="m-portlet__head-text">
                                            Page Views
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__body">
                                <div id="chartdiv" style="width: 100%; height: 500px;"></div>
                            </div>
                        </div>
                        <!--end:: Widgets/Latest Updates-->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>

        $(function(){
            chart();
            setInterval(chart, 300000)
        });

        function chart(){
            var url = base_url + '/backend/chart';
            $('#label_chart').html('');

            $.ajax({
                url: url,
                type: 'GET',
                success: function(res){
                    var browser = res.pie_chart;
                    var visitor = res.visitor;
                    pie_chart(browser);
                    visitor_chart(visitor);
                    browser.forEach(function(val){
                        var data = '<div class="m-widget14__legend">\
                                        <span class="m-widget14__legend-bullet m--bg-'+ val.color +'"></span>\
                                        <span class="m-widget14__legend-text">\
                                            '+ val.name +'\
                                        </span>\
                                    </div>';

                        $('#label_chart').append(data);
                    });
                }
            })
            
        }

        function pie_chart(browser) {
            if ($('#chart_pelanggan').length == 0) {
                return;
            }

            var data_browser = [];

            browser.forEach(function(val){
                data_browser.push ( {
                    value: val.value,
                    className: val.name,
                    meta: {
                        color: mUtil.getColor(val.color)
                    }
                });
            })

            var chart = new Chartist.Pie('#chart_pelanggan', {
                series: data_browser
            }, {
                donut: true,
                donutWidth: 17,
                showLabel: false
            });

            chart.on('draw', function(data) {
                if (data.type === 'slice') {
                    // Get the total path length in order to use for dash array animation
                    var pathLength = data.element._node.getTotalLength();

                    // Set a dasharray that matches the path length as prerequisite to animate dashoffset
                    data.element.attr({
                        'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
                    });

                    // Create animation definition while also assigning an ID to the animation for later sync usage
                    var animationDefinition = {
                        'stroke-dashoffset': {
                            id: 'anim' + data.index,
                            dur: 1000,
                            from: -pathLength + 'px',
                            to: '0px',
                            easing: Chartist.Svg.Easing.easeOutQuint,
                            // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
                            fill: 'freeze',
                            'stroke': data.meta.color
                        }
                    };

                    // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
                    if (data.index !== 0) {
                        animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
                    }

                    // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us

                    data.element.attr({
                        'stroke-dashoffset': -pathLength + 'px',
                        'stroke': data.meta.color
                    });

                    // We can't use guided mode as the animations need to rely on setting begin manually
                    // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
                    data.element.animate(animationDefinition, false);
                }
            });

            // For the sake of the example we update the chart every time it's created with a delay of 8 seconds
            chart.on('created', function() {
                if (window.__anim21278907124) {
                    clearTimeout(window.__anim21278907124);
                    window.__anim21278907124 = null;
                }
                window.__anim21278907124 = setTimeout(chart.update.bind(chart), 15000);
            });
        }

        function visitor_chart(visitor){

            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            var chart = am4core.create("chartdiv", am4charts.XYChart);
            var data_visitor = [];

            visitor.forEach(function(val){
                data_visitor.push ( {
                  "date": val.date,
                  "value": val.pageViews
                });
            })
            
            // Add data
            chart.data = data_visitor;

            // Set input format for the dates
            chart.dateFormatter.inputDateFormat = "yyyy-MM-dd";

            // Create axes
            var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

            // Create series
            var series = chart.series.push(new am4charts.LineSeries());
            series.dataFields.valueY = "value";
            series.dataFields.dateX = "date";
            series.tooltipText = "{value}"
            series.strokeWidth = 2;
            series.minBulletDistance = 15;

            // Drop-shaped tooltips
            series.tooltip.background.cornerRadius = 20;
            series.tooltip.background.strokeOpacity = 0;
            series.tooltip.pointerOrientation = "vertical";
            series.tooltip.label.minWidth = 40;
            series.tooltip.label.minHeight = 40;
            series.tooltip.label.textAlign = "middle";
            series.tooltip.label.textValign = "middle";

            // Make bullets grow on hover
            var bullet = series.bullets.push(new am4charts.CircleBullet());
            bullet.circle.strokeWidth = 2;
            bullet.circle.radius = 4;
            bullet.circle.fill = am4core.color("#fff");

            var bullethover = bullet.states.create("hover");
            bullethover.properties.scale = 1.3;

            // Make a panning cursor
            chart.cursor = new am4charts.XYCursor();
            chart.cursor.behavior = "panXY";
            chart.cursor.xAxis = dateAxis;
            chart.cursor.snapToSeries = series;

            // Create vertical scrollbar and place it before the value axis
            chart.scrollbarY = new am4core.Scrollbar();
            chart.scrollbarY.parent = chart.leftAxesContainer;
            chart.scrollbarY.toBack();

            // Create a horizontal scrollbar with previe and place it underneath the date axis
            chart.scrollbarX = new am4charts.XYChartScrollbar();
            chart.scrollbarX.series.push(series);
            chart.scrollbarX.parent = chart.bottomAxesContainer;

            chart.events.on("ready", function () {
              dateAxis.zoom({start:0.79, end:1});
            });
        }
    </script>
@endpush