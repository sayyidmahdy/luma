@extends('template.backend.index')
@section('content')

	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">
			<div class="m-portlet__body">
				<!--begin: Search Form -->
				<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
					<div class="row align-items-center">
						<div class="col-xl-12 order-1 order-xl-2 m--align-right">
							<a href="javascript:void(0)" onclick="modal_show('add')" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
								<span>
									<i class="la la-plus"></i>
									<span>
										Create
									</span>
								</span>
							</a>
							<div class="m-separator m-separator--dashed d-xl-none"></div>
						</div>
					</div>
				</div>
				<!--end: Search Form -->
				<!--begin: Datatable -->
				<table id="table_history_upload" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>No</th>
							<th>Name</th>
							<th width="20%">Description</th>
							<th>Status</th>
							<th>Publish</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
				<!--end: Datatable -->
			</div>
		</div>
	</div>

@endsection
@push('script')
    <script type="text/javascript">
        $(function(){
            table_testimonial();
        });

        function modal_show(act, id = ''){
   
   	    	var modal_url = (act == 'add') ? base_url + '/backend/testimonial/form/' + act : base_url + '/backend/testimonial/form/' + act + '/' + id;
   	    	var footer 	  = '<button type="button" class="btn btn-success" id="simpan" onclick="submit_testimonial(\''+act+'\', \''+id+'\')">Simpan</button>'
   	    	Modal('detail_notifikasi', 'Form Testimonial', modal_url , footer, 'modal-lg');
   	    }

        function table_testimonial(){
        	var url = base_url + '/backend/testimonial/datatables';
            var table = $('#table_history_upload').DataTable({ 
	            processing: true, 
	            serverSide: true, 
	            searching: true, 
	            destroy: true,
	            scrollX: true,
	            sort: false, 
	            order: [0, 'false'],
	            ajax: url,
	            columns: [
	                {"data": "rownum", "width": "5%"},
	                {"data": "name"},
	                {"data": "description", "width": "5%"},
	                {"data": "status", render: function(data, type, row)
	                	{
		                	var type = {
				              1: {'title': 'Verified', 'class': 'm-badge--primary'},
				              2: {'title': 'Not Verified', 'class': ' m-badge--danger'}
				            };
				            return '<span class="m-badge ' + type[data].class + ' m-badge--wide">' + type[data].title + '</span>';
		                }
		            },
	                {"data": "is_publish", render: function(data, type, row)
	                	{
		                	var type = {
				              'Y': {'title': 'Published', 'class': 'm-badge--success'},
				              'N': {'title': 'Not Publish', 'class': ' m-badge--danger'}
				            };
				            return '<span class="m-badge ' + type[data].class + ' m-badge--wide">' + type[data].title + '</span>';
		                }
		            },
	                {"data": "id_testimoni", render: function(data, type, row)
		                {
		                	var act = "'edit'";
		                	var act_status = "'status'";
		                	var act_publish = "'publish'";
		                	var val_status = row.status;
		                	var val_publish = "'"+row.is_publish+"'";
		                	var act_publish = "'publish'";

		                	var status = (row.status == 1) ? 'Unverified' : 'Set Verified';
		                	var publish = (row.is_publish == 'Y') ? 'Unpublish' : 'Set Publish';
	                		return '\
								<div class="dropdown">\
									<a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown">\
		                                <i class="la la-ellipsis-h"></i>\
		                            </a>\
								  	<div class="dropdown-menu dropdown-menu-right">\
								    	<a class="dropdown-item" href="javascript:void(0)" onclick="modal_show('+act+', '+data+')"><i class="la la-edit"></i> Edit Data</a>\
								    	<a class="dropdown-item" href="javascript:void(0)" onclick="delete_testimoni('+data+')"><i class="la la-trash"></i> Delete</a>\
								    	<a class="dropdown-item" href="javascript:void(0)" onclick="option_testimoni('+act_status+', '+data+', '+val_status+')"><i class="la la-leaf"></i> '+ status +'</a>\
								    	<a class="dropdown-item" href="javascript:void(0)" onclick="option_testimoni('+act_publish+', '+data+', '+val_publish+')"><i class="la la-print"></i> '+ publish +'</a>\
								  	</div>\
								</div>\
							';
		                }
		            }
	            ],

	        });
        }

        function submit_testimonial(act, id =''){
	        var validation = input_validator('form-testimonial');

	        if (validation.fail) {
	            swal('', 'Isian form belum lengkap atau belum benar', 'error');
	            return false;
            }

        	var url = (act == 'add') ? base_url + '/backend/testimonial/submit/' + act : base_url + '/backend/testimonial/submit/' + act + '/' + id;
	    	var file = document.getElementById('testimonial');
	    	
            var dataSend = new FormData();
            // dataSend.append("file", file.files[0]);
            dataSend.append("name", $('#name').val());
            dataSend.append("description", $('#description').val());

            $.ajax({
            	url: url,
            	type: 'POST',
            	data: dataSend,
            	processData: false,
	            contentType: false,
            	success: function(res){
            		ajax_alert(res);
            		var msg = res.split('|');
            		
                	if (msg[0] == 'success') {
                        $('#detail_notifikasi').modal('toggle');
                        
                    	table_testimonial();

                	}
            	}
            })

        }

        function option_testimoni(type, id, value){
        	var url = base_url + '/backend/testimonial/option/' + type + '/' + id + '/' + value;
        	Swal({
              title: '',
              text: "Are you sure?",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, do it!'
            }).then((result) => {
              if (result.value) {
                $.get(url, function(res){
	        		ajax_alert(res);
	        		table_testimonial();
	        	});
              }
            })   
        	
        }

        function delete_testimoni(id){
        	var url = base_url + '/backend/testimonial/submit/delete/' + id;
        	Swal({
              title: '',
              text: "Are you sure wants to remove this testimoni?",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, do it!'
            }).then((result) => {
              if (result.value) {
                $.post(url, function(res){
	        		ajax_alert(res);
	        		table_testimonial();
	        	});
              }
            })   
        	
        }
    </script>
@endpush