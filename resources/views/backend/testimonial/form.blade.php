
<div class="m-content">
	<!--begin::Portlet-->
	<div class="m-portlet">
		<!--begin::Form-->
		<form class="m-form m-form--state" id="form-testimonial" action="{{ url('backend/testimonial/submit/') }}" method="POST" enctype="multipart/form-data">
			@csrf
			<div class="m-portlet__body">
				<div class="m-form__section m-form__section--first">
					<!-- <div class="form-group m-form__group row">
						<label class="col-lg-3 col-md-3 col-sm-12 col-xs-12  col-form-label">
							User Image:
						</label>
						<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
							<input type="file" class="dropify" id="testimonial" name="image" data-max-file-size="3M" data-allowed-file-extensions="jpg png jpeg" required>
							<small><span class="form-control-feedback"></span></small>
						</div>
					</div> -->
					<div class="form-group m-form__group row">
						<label class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-form-label">
							Name:
						</label>
						<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
							<input type="text" class="form-control form-control-sm" id="name" name="name" value="{{ ($act =='edit') ? $data->name : ''}}" required>
							<small><span class="form-control-feedback"></span></small>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-form-label">
							Description:
						</label>
						<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
							<textarea class="form-control form-control-sm" id="description" name="description" required>{{ ($act =='edit') ? $data->description : ''}}</textarea>
							<small><span class="form-control-feedback"></span></small>
						</div>
					</div>
				</div>
			</div>
		</form>
		<!--end::Form-->
	</div>
	<!--end::Portlet-->
</div>
<script type="text/javascript">
	$(function(){
		exec_dropify();
	});
</script>