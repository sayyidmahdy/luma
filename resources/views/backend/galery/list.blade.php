@extends('template.backend.index')
@section('content')

	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">
			<div class="m-portlet__body">
				<!--begin: Search Form -->
				<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
					<div class="row align-items-center">
						<div class="col-xl-12 order-1 order-xl-2 m--align-right">
							<a href="{{ url('backend/galery/form/add') }}" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
								<span>
									<i class="la la-plus"></i>
									<span>
										Create
									</span>
								</span>
							</a>
							<div class="m-separator m-separator--dashed d-xl-none"></div>
						</div>
					</div>
				</div>
				<!--end: Search Form -->
				<!--begin: Datatable -->
				<table id="table_galery" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>No</th>
							<th>Title</th>
							<th>Total File</th>
							<th>Type</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
				<!--end: Datatable -->
			</div>
		</div>
	</div>
@endsection
@push('script')
    <script type="text/javascript">
        $(function(){
            table_galery();
        });

        function modal_show(act, id){
   
   	    	var modal_url =  base_url + '/backend/galery/form/' + act + '/' + id;
   	    	var footer 	  = '<button type="button" class="btn btn-success" id="simpan" onclick="submit_galery()">Simpan</button>'
   	    	Modal('detail_notifikasi', 'Form galery', modal_url , footer, 'modal-lg');
   	    }

        function table_galery(){
        	var url = base_url + '/backend/galery/datatables';
            var table = $('#table_galery').DataTable({ 
	            processing: true, 
	            serverSide: true, 
	            searching: true, 
	            scrollX: true,
	            destroy: true,
	            sort: false, 
	            order: [0, 'false'],
	            ajax: url,
	            columns: [
	                {"data": "rownum", "width": "5%"},
	                {"data": "title"},
	                {"data": "total_foto", render: function(data, type, row){
	                	return '<span class="m-badge m-badge--danger m-badge--wide">' + data + ' Photo & '+ row.total_video +' Video</span>';
	                }},
	                {"data": "type_id", render: function(data, type, row){
	                	var type = {
			              1: {'class': 'm-badge--brand'},
			              2: {'class': ' m-badge--metal'},
			              3: {'class': ' m-badge--primary'},
			              4: {'class': ' m-badge--success'},
			              5: {'class': ' m-badge--info'},
			              6: {'class': ' m-badge--danger'},
			              7: {'class': ' m-badge--warning'},
			            };
			            return '<span class="m-badge ' + type[data].class + ' m-badge--wide">' + row.type_name + '</span>';
	                }},
	                {"data": "id", render: function(data, type, row)
		                {
		                	var act = "'edit'";
		                	var url_item = base_url + '/backend/galery/add-item/' + data;

	                		return '\
								<div class="dropdown">\
									<a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown">\
		                                <i class="la la-ellipsis-h"></i>\
		                            </a>\
								  	<div class="dropdown-menu dropdown-menu-right">\
								    	<a class="dropdown-item" href="javascript:void(0)" onclick="modal_show('+act+', '+data+')"><i class="la la-edit"></i> Edit Data</a>\
								    	<a class="dropdown-item" href="'+ url_item +'" ><i class="la la-leaf"></i>Add Item</a>\
								    	<a class="dropdown-item" href="javascript:void(0)" onclick="delete_galery('+data+')"><i class="la la-edit"></i> Delete</a>\
								  	</div>\
								</div>\
							';
		                }
		            }

	            ],

	        });
        }

        function delete_galery(id){
        	var url = base_url + '/backend/galery/submit/delete/' + id;
        	Swal({
              title: '',
              text: "Are you sure wants to remove this galery?",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, do it!'
            }).then((result) => {
              if (result.value) {
                $.post(url, function(res){
	        		ajax_alert(res);
	        		table_galery();
	        	});
              }
            })   
        	
        }
    </script>
@endpush