@extends('template.backend.index')
@section('content')

<div class="m-content">
	<!--begin::Portlet-->
	<div class="m-portlet">
		<!--begin::Form-->
		<form class="m-form m-form--state" action="{{ url('backend/galery/submit-item/' . $id) }}" method="POST" enctype="multipart/form-data">
			@csrf
			<div class="m-portlet__body">
				<div class="m-form__section m-form__section--first">
					<div class="row">
						
						<div class="form-repeaters col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<div class="form-group  m-form__group row">
								<label  class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-form-label">
									Image:
								</label>
								<div data-repeater-list="image" class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
									<div data-repeater-item class="row m--margin-bottom-10">
										<div class="col-lg-10 col-md-10 col-sm-8 col-xs-8">
											<input type="file" class="form-control form-control-danger" name="file" data-max-size="32154" required>
										</div>
										<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4">
											<a href="javascript:void(0)" data-repeater-delete="" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only btn-lg">
												<i class="la la-remove"></i>
											</a>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>
								<div class="col">
									<div data-repeater-create="" class="btn btn btn-primary m-btn m-btn--icon">
										<span>
											<i class="la la-plus"></i>
											<span>
												Add
											</span>
										</span>
									</div>
								</div>
							</div>
						</div>
						<div class="form-repeaters col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<div class="form-group  m-form__group row">
								<label  class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-form-label">
									Video:
								</label>
								<div data-repeater-list="video" class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
									<div data-repeater-item class="row m--margin-bottom-10">
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
											<input type="text" class="form-control form-control-danger" name="vidio">
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
											<select class="form-control form-control-danger" name="type" required>
												@foreach($sosmed as $row)
												<option value="{{ $row->id_sosmed }}">{{ $row->name }}</option>
												@endforeach
											</select>
										</div>
										<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
											<a href="javascript:void(0)" data-repeater-delete="" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only btn-lg">
												<i class="la la-remove"></i>
											</a>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>
								<div class="col">
									<div data-repeater-create="" class="btn btn btn-primary m-btn m-btn--icon">
										<span>
											<i class="la la-plus"></i>
											<span>
												Add
											</span>
										</span>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
			<div class="m-portlet__foot m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions">
					<div class="row">
						<div class="col-lg-2"></div>
						<div class="col">
							<button type="submit" class="btn btn-success">
								Submit
							</button>
							<a href="{{ url('backend/galery') }}" class="btn btn-secondary">
								Cancel
							</a>
						</div>
					</div>
				</div>
			</div>
		</form>
		<!--end::Form-->
	</div>
	<!--end::Portlet-->
</div>

@endsection
@push('script')

@endpush