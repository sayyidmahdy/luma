<div class="m-content">
	<!--begin::Portlet-->
	<div class="m-portlet">
		<!--begin::Form-->
		<form class="m-form m-form--state" action="{{ url('backend/galery/submit/edit/' . $id) }}" method="POST" enctype="multipart/form-data">
			@csrf
			<div class="m-portlet__body">
				<div class="m-form__section m-form__section--first">
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-md-2 col-sm-12 col-xs-12  col-form-label">
							Title:
						</label>
						<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
							<input type="text" class="form-control m-input" name="data[title]" value="{{ $data->title }}" placeholder="Enter Title here" required>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-md-2 col-sm-12 col-xs-12  col-form-label">
							Type:
						</label>
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
							<select class="form-control form-control-danger" name="data[type_id]" required>
								@foreach($type as $row)
								<option value="{{ $row->id_type }}" {{ ($data->type_id == $row->id_type) ? 'selected' : '' }}>{{ $row->name }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-md-2 col-sm-12 col-xs-12  col-form-label">
							Thumbnail:
						</label>
						<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
							<input type="file" class="dropify" name="thumbnail" data-max-file-size="3M" data-allowed-file-extensions="jpg png jpeg" data-default-file="{{ ($data->thumbnail == NULL) ? '' : asset('/upload/galery/'.$id.'/thumbnail/' . $data->thumbnail) }}">
						</div>
					</div>
				</div>
			</div>
			<button type="submit" style="display: none;" id="btn-galery"></button>
		</form>
	</div>
</div>

<script>
	$(function(){
		exec_dropify();
	});

	function submit_galery(){
		$('#btn-galery').click();
	}
</script>