
<div class="m-content">
	<!--begin::Portlet-->
	<div class="m-portlet">
		<!--begin::Form-->
		<form class="m-form m-form--state" id="form-slideshow" action="{{ url('backend/slideshow/submit/add') }}" method="POST" enctype="multipart/form-data">
			@csrf
			<input type="hidden" id="id" value="{{ ($act == 'add') ? '' : $id }}">
			<input type="hidden" id="act" value="{{ $act }}">
			<div class="m-portlet__body">
				<div class="m-form__section m-form__section--first">
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-md-2 col-sm-12 col-xs-12  col-form-label">
							Type:
						</label>
						<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12" >
							<select class="form-control" id="type" name="type" onchange="type_slideshow(this.value)">
								@if($act == 'add')
									<option value="image">Image</option>
									<option value="video">Video</option>
								@else
									<option value="image" {{ ($data->type == 'image') ? 'selected' : '' }}>Image</option>
									<option value="video" {{ ($data->type == 'video') ? 'selected' : '' }}>Video</option>
								@endif
							</select>
						</div>
					</div>
					<div class="form-group m-form__group row" id="row_video" style="display: none;">
						<label class="col-lg-2 col-md-2 col-sm-12 col-xs-12 col-form-label">
							URL Video:
						</label>
						<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
							<input type="text" class="form-control" id="link_url" name="url" value="{{ ($act == 'edit') ? $data->url : '' }}">
						</div>
					</div>
					<div class="form-group m-form__group row" id="row_image">
						<label class="col-lg-2 col-md-2 col-sm-12 col-xs-12  col-form-label">
							Slideshow:
						</label>
						<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
							@if($act == 'add')
								<input type="file" class="dropify" id="slideshow" name="image" data-max-file-size="3M" data-allowed-file-extensions="jpg png jpeg" required>
							@else
								<input type="file" class="dropify" id="slideshow" name="image" data-max-file-size="3M" data-allowed-file-extensions="jpg png jpeg" data-default-file="{{ ($data->file == NULL || $data->file == '') ? '' : asset('/upload/slideshow/' . $data->file) }}" >
							@endif
						</div>
					</div>
				</div>
			</div>
		</form>
		<!--end::Form-->
	</div>
	<!--end::Portlet-->
</div>
<script type="text/javascript">
	$(function(){
		exec_dropify();
		
		@if($act == 'edit')
			type_slideshow('{{ $data->type }}');
		@endif
	});

	function type_slideshow(type){
		var act = $('#act').val();
		if (type == 'video') {
			$('#link_url').prop('required', true);	
			$('#row_video').show();
		}
		else {
			$('#row_video').hide();
			$('#link_url').prop('required', false);	
			$('#row_image').show();

			if (act == 'add') {
				$('#slideshow').prop('required', true);
			}
		}
	}
</script>