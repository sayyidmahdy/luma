@extends('template.backend.index')
@section('content')

	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">
			<div class="m-portlet__body">
				<!--begin: Search Form -->
				<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
					<div class="row align-items-center">
						<div class="col-xl-8 order-2 order-xl-1">
							<div class="form-group m-form__group row align-items-center">
								<div class="col-md-4">
									<div class="m-input-icon m-input-icon--left">
										<input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="generalSearch">
										<span class="m-input-icon__icon m-input-icon__icon--left">
											<span>
												<i class="la la-search"></i>
											</span>
										</span>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-4 order-1 order-xl-2 m--align-right">
							<a href="javascript:void(0)" onclick="modal_show('add')" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
								<span>
									<i class="la la-plus"></i>
									<span>
										Create
									</span>
								</span>
							</a>
							<div class="m-separator m-separator--dashed d-xl-none"></div>
						</div>
					</div>
				</div>
				<!--end: Search Form -->
				<!--begin: Datatable -->
				<table id="table_history_upload" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>No</th>
							<th>File</th>
							<th>Url</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
				<!--end: Datatable -->
			</div>
		</div>
	</div>
@endsection
@push('script')
    <script type="text/javascript">
        $(function(){
            table_slideshow();
        });

        function modal_show(act, id =''){
   			
   			if (act == 'add') {
	   	    	var modal_url =  base_url + '/backend/slideshow/form/add';
   			}
   			else {
	   	    	var modal_url =  base_url + '/backend/slideshow/form/edit/' + id;
   			}

   	    	var footer 	  = '<button type="button" class="btn btn-success" id="simpan" onclick="submit_slideshow()">Simpan</button>'
   	    	Modal('detail_notifikasi', 'Form Slideshow', modal_url , footer, 'modal-lg');
   	    }

        function table_slideshow(){
        	var url = base_url + '/backend/slideshow/datatables';
            var table = $('#table_history_upload').DataTable({ 
	            processing: true, 
	            serverSide: true, 
	            searching: false, 
	            destroy: true,
	            scrollX: true,
	            sort: false, 
	            order: [0, 'false'],
	            ajax: url,
	            columns: [
	                {"data": "rownum", "width": "5%"},
	                {"data": "file"},
	                {"data": "url"},
	                {"data": "id_slideshow", render: function(data, type, row)
		                {
		                	var url_item = base_url + '/backend/slideshow/add-item/' + data;
		                	var act = "'edit'";
	                		return '\
								<div class="dropdown">\
									<a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown">\
		                                <i class="la la-ellipsis-h"></i>\
		                            </a>\
								  	<div class="dropdown-menu dropdown-menu-right">\
								    	<a class="dropdown-item" href="javascript:void(0)" onclick="modal_show('+act+', '+data+')"><i class="la la-edit"></i> Edit</a>\
								    	<a class="dropdown-item" href="javascript:void(0)" onclick="delete_slideshow('+data+')"><i class="la la-trash"></i> Delete</a>\
								  	</div>\
								</div>\
							';
		                }
		            }
	            ],

	        });
        }

        function submit_slideshow(){
        	var validation = input_validator('form-slideshow');
        	var act = $('#act').val();
        	var id = $('#id').val();

	        if (validation.fail) {
	            swal('', 'Isian form belum lengkap atau belum benar', 'error');
	            return false;
            }

            if (act == 'add') {
	        	var url = base_url + '/backend/slideshow/submit/add';
            }
            else {
	        	var url = base_url + '/backend/slideshow/submit/edit/' + id;
            }

	    	var file = document.getElementById('slideshow');
	    	var type = $('#type').val();
	    	
            var dataSend = new FormData();
        	dataSend.append("type", type);
        	dataSend.append("file", file.files[0]);

            if (type == 'video') {
            	dataSend.append("url", $('#link_url').val());
            }

            $.ajax({
            	url: url,
            	type: 'POST',
            	data: dataSend,
            	processData: false,
	            contentType: false,
            	success: function(res){
            		var msg = res.split('|');

                	setTimeout(function() {
		                showFlashAlert(msg[0], msg[1]);
		            }, 1000);

                    $('#detail_notifikasi').modal('toggle');
                    
                	table_slideshow();
            	}
            })

        }

        function delete_slideshow(id){
        	var url = base_url + '/backend/slideshow/submit/delete/' + id;
        	Swal({
              title: '',
              text: "Are you sure wants to remove this slideshow?",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, do it!'
            }).then((result) => {
              if (result.value) {
                $.post(url, function(res){
	        		ajax_alert(res);
	        		table_slideshow();
	        	});
              }
            })   
        	
        }
    </script>
@endpush