@extends('template.backend.index')
@section('content')

	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">
			<div class="m-portlet__body">
				<!--begin: Datatable -->
				<table id="table_history_upload" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Action</th>
							<th>No</th>
							<th>Name</th>
				            <th>Email</th>
				            <th>Telephone</th>
				            <th>Venue</th>
							<th>Message</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
				<!--end: Datatable -->
			</div>
		</div>
	</div>
@endsection
@push('script')
    <script type="text/javascript">
        $(function(){
            table_contact();
        });

        function modal_show(act, id = ''){
   
   	    	var modal_url = (act == 'add') ? base_url + '/backend/contact/form/' + act : base_url + '/backend/contact/form/' + act + '/' + id;
   	    	var footer 	  = '<button type="button" class="btn btn-success" id="simpan" onclick="submit_contact('+act+', '+id+')">Simpan</button>'
   	    	Modal('detail_notifikasi', 'Form contact', modal_url , footer, 'modal-lg');
   	    }

        function table_contact(){
        	var url = base_url + '/backend/contact/datatables';
            var table = $('#table_history_upload').DataTable({ 
	            processing: true, 
	            serverSide: true, 
	            searching: true, 
	            destroy: true,
              scrollX: true,
	            sort: false, 
	            order: [0, 'false'],
	            ajax: url,
	            columns: [
	            	{"data": "id_contact", render: function(data, type, row)
		                {
		                	var url_item = base_url + '/backend/slideshow/add-item/' + data;
		                	var act = "'edit'";
	                		return '\
								    	<a class="btn btn-sm btn-danger" href="javascript:void(0)" onclick="deletes('+data+')"><i class="la la-trash"></i> Delete</a>\
							';
		                }
		            },
	                {"data": "rownum", "width": "5%"},
	                {"data": "name"},
	                {"data": "email"},
	                {"data": "telephone"},
	                {"data": "venue"},
	                {"data": "message"}
	            ],

	        });
        }

        function submit_contact(act, id =''){
	        var validation = input_validator('form-contact');

	        if (validation.fail) {
	            swal('', 'Isian form belum lengkap atau belum benar', 'error');
	            return false;
            }

        	var url = (act == 'add') ? base_url + '/backend/contact/submit/' + act : base_url + '/backend/contact/submit/' + act + '/' + id;
	    		var file = document.getElementById('contact');
	    	
            var dataSend = new FormData();
            dataSend.append("file", file.files[0]);
            dataSend.append("name", $('#name').val());
            dataSend.append("description", $('#description').val());

            $.ajax({
            	url: url,
            	type: 'POST',
            	data: dataSend,
            	processData: false,
	            contentType: false,
            	success: function(res){
            		ajax_alert(res);

                	if (msg[0] == 'success') {
                        $('#detail_notifikasi').modal('toggle');
                        
                    	table_contact();

                	}
            	}
            })

        }

        function option_testimoni(type, id, value){
        	var url = base_url + '/backend/contact/option/' + type + '/' + id + '/' + value;
        	Swal({
              title: '',
              text: "Are you sure?",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, do it!'
            }).then((result) => {
              if (result.value) {
                $.get(url, function(res){
			        		ajax_alert(res);
			        		table_contact();
			        	});
              }
            })   
        	
        }

        function deletes(id){
        	var url = base_url + '/backend/contact/delete/' + id;
        	Swal({
              title: '',
              text: "Are you sure wants to remove this slideshow?",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, do it!'
            }).then((result) => {
              if (result.value) {
                $.post(url, function(res){
		        		ajax_alert(res);
		        		table_contact();
	        			});
              }
            });  
        }
    </script>
@endpush