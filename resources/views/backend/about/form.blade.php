
<div class="m-content">
	<!--begin::Portlet-->
	<div class="m-portlet">
		<!--begin::Form-->
		<form class="m-form m-form--state" id="form-about" action="{{ url('backend/about/submit/') }}" method="POST" enctype="multipart/form-data">
			@csrf
			<div class="m-portlet__body">
				<div class="m-form__section m-form__section--first">
					<div class="form-group m-form__group row">
						<label class="col-lg-3 col-md-3 col-sm-12 col-xs-12  col-form-label">
							User Image:
						</label>
						<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
							@if($act == 'add')
								<input type="file" class="dropify" id="about" name="image" data-max-file-size="3M" data-allowed-file-extensions="jpg png jpeg" required>
							@else
								<input type="file" class="dropify" id="about" name="about" data-max-file-size="3M" data-allowed-file-extensions="jpg png jpeg" data-default-file="{{ ($data->image == NULL) ? '' : asset('/upload/about/' . $data->image) }}">
							@endif
							<small><span class="form-control-feedback"></span></small>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-form-label">
							Name:
						</label>
						<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
							<input type="text" class="form-control form-control-sm" id="name" name="name" value="{{ ($act =='edit') ? $data->name : ''}}" required>
							<small><span class="form-control-feedback"></span></small>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-form-label">
							Position:
						</label>
						<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
							<input type="text" class="form-control form-control-sm" id="position" name="position" value="{{ ($act =='edit') ? $data->position : ''}}" required>
							<small><span class="form-control-feedback"></span></small>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-form-label">
							Facebook:
						</label>
						<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
							<input type="text" class="form-control form-control-sm" id="facebook" name="facebook" value="{{ ($act =='edit') ? $data->facebook : ''}}" required>
							<small><span class="form-control-feedback"></span></small>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-form-label">
							Instagram:
						</label>
						<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
							<input type="text" class="form-control form-control-sm" id="instagram" name="instagram" value="{{ ($act =='edit') ? $data->instagram : ''}}" required>
							<small><span class="form-control-feedback"></span></small>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-form-label">
							Twitter:
						</label>
						<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
							<input type="text" class="form-control form-control-sm" id="twitter" name="twitter" value="{{ ($act =='edit') ? $data->twitter : ''}}" required>
							<small><span class="form-control-feedback"></span></small>
						</div>
					</div>
				</div>
			</div>
		</form>
		<!--end::Form-->
	</div>
	<!--end::Portlet-->
</div>
<script type="text/javascript">
	$(function(){
		exec_dropify();
	});
</script>