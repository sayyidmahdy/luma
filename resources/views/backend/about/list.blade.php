@extends('template.backend.index')
@section('content')

	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">
			<div class="m-portlet__body">
				<!--begin: Search Form -->
				<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
					<div class="row align-items-center">
						<div class="col-xl-12 order-1 order-xl-2 m--align-right">
							<a href="javascript:void(0)" onclick="modal_show('add')" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
								<span>
									<i class="la la-plus"></i>
									<span>
										Create
									</span>
								</span>
							</a>
							<div class="m-separator m-separator--dashed d-xl-none"></div>
						</div>
					</div>
				</div>
				<!--end: Search Form -->
				<!--begin: Datatable -->
				<table id="table_about" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>No</th>
							<th>Name</th>
							<th>Position</th>
							<th>Facebook</th>
							<th>Instagram</th>
							<th>Twitter</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
				<!--end: Datatable -->
			</div>
		</div>
	</div>
@endsection
@push('script')
    <script type="text/javascript">
        $(function(){
            table_about();
        });

        function modal_show(act, id = ''){
   
   	    	var modal_url = (act == 'add') ? base_url + '/backend/about/form/' + act : base_url + '/backend/about/form/' + act + '/' + id;
   	    	var footer 	  = '<button type="button" class="btn btn-success" id="simpan" onclick="submit_about(\''+act+'\', \''+id+'\')">Simpan</button>'
   	    	Modal('detail_notifikasi', 'Form About Us', modal_url , footer, 'modal-lg');
   	    }

        function table_about(){
        	var url = base_url + '/backend/about/datatables';
            var table = $('#table_about').DataTable({ 
	            processing: true, 
	            serverSide: true, 
	            searching: true, 
	            destroy: true,
	            scrollX: true,
	            sort: false, 
	            order: [0, 'false'],
	            ajax: url,
	            columns: [
	                {"data": "rownum", "width": "5%"},
	                {"data": "name"},
	                {"data": "position"},
	                {"data": "facebook"},
	                {"data": "instagram"},
	                {"data": "twitter"},
	                {"data": "id", render: function(data, type, row)
		                {
		                	var act = "'edit'";
		                	var url_item = base_url + '/backend/about/add-item/' + data;

	                		return '\
								<div class="dropdown">\
									<a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown">\
		                                <i class="la la-ellipsis-h"></i>\
		                            </a>\
								  	<div class="dropdown-menu dropdown-menu-right">\
								    	<a class="dropdown-item" href="javascript:void(0)" onclick="modal_show('+act+', '+data+')"><i class="la la-edit"></i> Edit Data</a>\
								    	<a class="dropdown-item" href="javascript:void(0)" onclick="delete_about('+data+')"><i class="la la-trash"></i> Delete</a>\
								  	</div>\
								</div>\
							';
		                }
		            }

	            ],

	        });
        }

        function delete_about(id){
        	var url = base_url + '/backend/about/submit/delete/' + id;
        	Swal({
              title: '',
              text: "Are you sure wants to remove this about?",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, do it!'
            }).then((result) => {
              if (result.value) {
                $.post(url, function(res){
	        		ajax_alert(res);
	        		table_about();
	        	});
              }
            })   
        	
        }

        function submit_about(act, id =''){
	        var validation = input_validator('form-about');

	        if (validation.fail) {
	            swal('', 'Isian form belum lengkap atau belum benar', 'error');
	            return false;
            }

        	var url = (act == 'add') ? base_url + '/backend/about/submit/' + act : base_url + '/backend/about/submit/' + act + '/' + id;
	    	var file = document.getElementById('about');
	    	
            var dataSend = new FormData();
            dataSend.append("file", file.files[0]);
            dataSend.append("name", $('#name').val());
            dataSend.append("position", $('#position').val());
            dataSend.append("facebook", $('#facebook').val());
            dataSend.append("instagram", $('#instagram').val());
            dataSend.append("twitter", $('#twitter').val());

            $.ajax({
            	url: url,
            	type: 'POST',
            	data: dataSend,
            	processData: false,
	            contentType: false,
            	success: function(res){
            		ajax_alert(res);
            		var msg = res.split('|');
            		
                	if (msg[0] == 'success') {
                        $('#detail_notifikasi').modal('toggle');
                        
                    	table_about();

                	}
            	}
            })

        }

        function delete_about(id){
        	var url = base_url + '/backend/about/submit/delete/' + id;
        	Swal({
              title: '',
              text: "Are you sure wants to remove this about?",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, do it!'
            }).then((result) => {
              if (result.value) {
                $.post(url, function(res){
	        		ajax_alert(res);
	        		table_about();
	        	});
              }
            })   
        	
        }
    </script>
@endpush