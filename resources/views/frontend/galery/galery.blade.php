@extends('template.frontend.index')
@section('content')
	<section class="height-70 bg--dark imagebg page-title page-title--animate parallax" data-overlay="6">
	    <div class="background-image-holder">
	        <img alt="image" src="{{ asset('frontend/img/hero16.jpg')}}" />
	    </div>
	    <!--end container-->
	</section>

	<section class="height-30" style="margin-top: 5rem">
        <div class="container pos-vertical-center">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 text-center">
                    <h1>{{ ucfirst($category) }}</h1>
                    <p class="lead">
                        Every couple is unique Let's inspire the world with your stories Let's collect moments as reminders of how much we're loved
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row">
                @foreach($galery as $row)
                <div class="col-md-4 col-sm-12">
                    <a href="{{ url('/' . $category . '/' .$row->slug) }}">
                        <div class="card card-3">
                            <div class="card__image">
                                <img alt="Pic" src="{{ asset('/upload/galery/' . $row->id . '/thumbnail/' . $row->thumbnail) }}" />
                            </div>
                            <div class="card__body boxed bg--white">
                                <div class="card__title">
                                    <h5>{{ $row->type_name }}</h5>
                                </div>
                                <span>
                                    <em>{{ $row->title }}</em>
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
            <!--end row-->
        </div>
        <!--end of container-->
    </section>
@endsection