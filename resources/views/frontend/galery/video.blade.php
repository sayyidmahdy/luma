
@extends('template.frontend.index')
@section('content')
    <section class="imagebg height-50">
    <div class="background-image-holder background--bottom">
        <img alt="image" src="{{ asset('frontend/img/header2.jpg')}}" />
    </div>
    <div class="container pos-vertical-center">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h1>Video Showcase</h1>
                <p class="lead">Showcase a video from luma story</p>
            </div>
        </div>
        <!--end row-->
    </div>
    <!--end container-->
</section>
<section class="masonry-contained">
    <div class="container">
        <div class="row">
            <div class="masonry">
                <div class="masonry__filters" data-filter-all-text="Show All"></div>
                <?php foreach ($video as $row): ?>
                    <div class="masonry__container masonry--animate">
                        <div class="col-sm-6 col-xs-12 masonry__item" data-masonry-filter="{{ $row->name }}">
                            <div class="portfolio-item portfolio-item-2 video-cover" data-scrim-bottom="9">
                                <div class="background-image-holder">
                                    <img alt="image" src="{{ asset('upload/video/' . $row->thumbnail) }}" />
                                </div>
                                <div class="portfolio-item__title">
                                    <h5>{{ $row->title }}</h5>
                                    <span>
                                        <em>{{ $row->name }}</em>
                                    </span>
                                </div>
                                <div class="video-play-icon video-play-icon--sm"></div>
                                <iframe src="{{ $row->url_video }}" allowfullscreen="allowfullscreen"></iframe>
                            </div>
                        </div>

                        <!--end item-->
                    </div>
                    <!--end masonry container-->
                <?php endforeach ?>
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>
@endsection