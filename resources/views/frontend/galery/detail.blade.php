@extends('template.frontend.index')
@section('content')
    <section class="height-100 imagebg parallax" data-overlay="3">
        <div class="background-image-holder">
            <!-- <img alt="background" src="https://iluminen.com/assets/file/2018/07/APW3318-scaled.jpg" /> -->
            <img alt="background" src="{{ asset('/upload/galery/' . $data->id . '/thumbnail/' . $data->thumbnail) }}" />
        </div>
        <div class="container pos-vertical-center">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h2 class="">{{ $data->title }}</h2>
                    <p>{{ $data->description }}</p>
                </div>
            </div>
            <!--eond of row-->
            </ditiv>
            <!--end of container-->
    </section>
    
	<section class="bg-soft-dark masonry-contained">
        <div class="container">
            <div class="row">
                <div class="masonry" style="margin-top: 0px !important">
                    <div class="masonry__filters text-center" data-filter-all-text="Show All"></div>
                    <div class="masonry__container masonry--gapless masonry--animate">
                        @foreach($image as $row)
                        <div class="col-sm-4 masonry__item">
                            <a data-lightbox="gallery" href="{{ asset('/upload/galery/' . $row->header_id . '/detail/' . $row->file) }}">
                                <div class="hover-element hover-element-1" data-title-position="top,right">
                                    <div class="hover-element__initial">
                                        <img alt="Pic" src="{{ asset('/upload/galery/' . $row->header_id . '/detail/' . $row->file) }}" />
                                    </div>
                                    <div class="hover-element__reveal" data-overlay="9">
                                        <div class="boxed">
                                            <!-- <h5>Freehance</h5>
                                            <span>
                                                <em>iOS Application</em>
                                            </span> -->
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    @if(count($video) > 0)
    <section class="masonry-contained">
        <div class="container">
            <div class="row">
                <div class="col-sm-offset-2 text-center col-sm-8">
                    <h3>Video</h3>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="masonry">
                    <div class="masonry__container masonry--animate">
                        @foreach($video as $row)
                        <div class="col-sm-6 col-xs-12 masonry__item">
                            <div class="portfolio-item portfolio-item-2 video-cover" data-scrim-bottom="9">
                                <div class="background-image-holder">
                                    <img alt="image" src="{{ asset('frontend/img/small2.jpg')}}" />
                                </div>
                                <div class="portfolio-item__title">
                                    <h5>Ennichisai 2018 | Luma Story</h5>
                                    <span>
                                        <em>Event</em>
                                    </span>
                                </div>
                                <div class="video-play-icon video-play-icon--sm"></div>
                                <iframe src="{{ $row->embed_url }}" allowfullscreen="allowfullscreen"></iframe>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif

    <section class="blog-post">
        <div class="blog-post__title bg--secondary">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1" id="comment">
                        <div class="blog-post__comments">
                            <hr>
                            <h6>{{ count($comment) }} Comments:</h6>
                            <ul>
                                @foreach($comment as $row)
                                <li>
                                    <div class="comment">
                                        <div class="comment__image">
                                            <img alt="pic" src="{{ asset('frontend/img/avatar-small-1.png') }}" />
                                        </div>
                                        <div class="comment__text">
                                            <h5>{{ $row->username }}</h5>
                                            <span>
                                                <em>{{ $row->new_date }}</em>
                                            </span>
                                            <p>
                                                {{ $row->comment }}
                                            </p>
                                        </div>
                                        <hr>
                                    </div>
                                    <!--end comment-->
                                    <ul>
                                        @foreach($reply as $row_reply)
                                            @if($row_reply->is_reply == $row->id_comment)
                                            <li>
                                                <div class="comment">
                                                    <div class="comment__image">
                                                        <img alt="pic" src="{{ asset('frontend/img/avatar-small-3.png') }}" />
                                                    </div>
                                                    <div class="comment__text">
                                                        <h5>{{ $row_reply->username }}</h5>
                                                        <span>
                                                            <em>{{ $row_reply->new_date }}</em>
                                                        </span>
                                                        <p>
                                                            {{ $row_reply->comment }}
                                                        </p>
                                                    </div>
                                                    <hr>
                                                </div>
                                                <!--end comment-->
                                            </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </li>
                                @endforeach
                            </ul>
                            <form action="{{ url('comment-submit') }}" method="POST" class="comment__form form--square">
                                @csrf
                                <h6>Leave a Comment</h6>
                                <input type="hidden" name="data[header_id]" value="{{ $header_id }}">
                                <input placeholder="Your Name" type="text" name="data[username]"  required/>
                                <input placeholder="Email Address" type="email" name="data[email]"  required/>
                                <textarea placeholder="Your Comment" name="data[comment]" rows="4" required></textarea>
                                <button type="submit" class="btn btn--primary">Submit Comment</button>
                            </form>
                        </div>
                        <!--end of blog comments-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <a href="#comment" id="comments" style="display:none;"></a>
@endsection