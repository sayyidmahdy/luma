@extends('template.frontend.index')
@section('content')

	<section class="slider slider--animate height-100 cover cover-5" data-arrows="true" data-paging="true" data-timing="4000">
	    <ul class="slides">
	        <!-- <li class="imagebg" data-overlay="4">
	            <div class="background-image-holder">
	                <img alt="image" src="https://iluminen.com/assets/file/2018/07/APW3318-scaled.jpg">
	            </div>
	        </li>
	        <li class="imagebg" data-overlay="4">
	            <div class="background-image-holder">
	                <img alt="image" src="https://iluminen.com/assets/file/2018/05/ETJI018-scaled.jpg">
	            </div>
	        </li>
	        <li class="imagebg" data-overlay="4">
	            <div class="background-image-holder">
	                <img alt="image" src="https://iluminen.com/assets/file/2018/07/ETJI056-1-scaled.jpg">
	            </div>
	        </li>
	        <li class="imagebg" data-overlay="4">
	            <div class="background-image-holder">
	                <img alt="image" src="https://iluminen.com/assets/file/2018/03/ETJI005-scaled.jpg">
	            </div>
	        </li> -->
            @foreach($slideshow as $row)
                @if($row->type == 'image')
    	        <li class="imagebg" data-overlay="4">
    	            <div class="background-image-holder">
    	                <img alt="image" src="{{ asset('upload/slideshow/' . $row->file) }}">
    	            </div>
    	        </li>
                @else 
                <li>
                    <div class="background-image-holder background--bottom">
                        <img alt="image" src="{{ asset('upload/slideshow/' . $row->file) }}" />
                    </div>
                    <div class="container pos-vertical-center">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <div>
                                    <div class="modal-instance modal-video-1">
                                        <div class="video-play-icon video-play-icon--sm modal-trigger"></div>
                                        <div class="modal-container">
                                            <div class="modal-content bg-dark" data-width="60%" data-height="60%">
                                                <iframe src="{{ $row->url }}" allowfullscreen="allowfullscreen"></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                @endif
            @endforeach
	    </ul>
	</section>
    <section class="height-100 imagebg cover cover-4" data-overlay="6" style="display: none;">
        <div class="background-image-holder background--bottom" >
            <img alt="image" src="https://iluminen.com/assets/file/2020/10/wonderland-uluwatu-melody-daniel-wedding-iluminen-bali-photography-junebug-photobug-dirtybootsmessyhair-destination-photographer055-1-scaled.jpg" />
        </div>
        <div class="container pos-vertical-center">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <!-- <h1>
                        About Luma Story
                    </h1> -->
                    <div>
                        <div class="modal-instance modal-video-1">
                            <div class="video-play-icon video-play-icon--sm modal-trigger"></div>
                            <div class="modal-container">
                                <div class="modal-content bg-dark" data-width="60%" data-height="60%">
                                    <iframe src="http://instagram.com/p/B5kInxRnEq7/embed" allowfullscreen="allowfullscreen"></iframe>
                                </div>
                                <!--end of modal-content-->
                            </div>
                            <!--end of modal-container-->
                        </div>
                        <!--end of modal instance-->
                    </div>
                </div>
            </div>
        </div>
    </section>

	<section class="wide-grid masonry">
        <div class="masonry__container masonry--animate" style="background: #333333 !important">
        	@foreach($galery as $row)
            <div class="col-sm-6 masonry__item" data-masonry-filter="branding">
                <a href="{{ url($row->type_name . '/' . $row->slug) }}">
                <!-- <a data-lightbox="gallery" href="{{ asset('/upload/galery/' . $row->id . '/thumbnail/' . $row->thumbnail) }}"> -->
                    <div class="hover-element hover-element-1" data-title-position="top,right">
                        <div class="hover-element__initial">
                            <img alt="Pic" src="{{ asset('/upload/galery/' . $row->id . '/thumbnail/' . $row->thumbnail) }}" />
                        </div>
                        <div class="hover-element__reveal" data-overlay="9">
                            <div class="boxed">
                                <h5>{{ $row->type_name }}</h5>
                                <span>
                                    <em>{{ $row->title }}</em>
                                </span>
                            </div>
                        </div>
                    </div>
                    <!--end hover element-->
                </a>
            </div>
            @endforeach
        </div>
        <!--end masonry container-->
    </section>

    <section class="testimonial testimonial-4 section--even imagebg parallax" data-overlay="5">
        <div class="background-image-holder">
            <img alt="image" src="{{ asset('frontend/img/hero10.jpg')}}" />
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h3>Testimoni</h3>
                </div>
            </div>
            <!--end of row-->
            <div class="row">
                <div class="slider slider--animate slider--controlsoutside" data-animation="fade" data-arrows="false" data-paging="true" data-timing="5000">
                    <ul class="slides">
                    	@foreach($testimoni as $row)
                        <li>
                            <div class="col-sm-10 col-sm-offset-1 text-center">
                                <blockquote>
                                    &ldquo;{{ $row->description }}&rdquo;
                                </blockquote>
                                <h5>&mdash; {{ $row->name }}</h5>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section class="blog-post pb-0">
        <div class="blog-post__title bg--secondary">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1" id="comment">
                        <div class="blog-post__comments">
                            <hr>
                            <h6>{{ count($comment) }} Comments:</h6>
                            <ul>
                                @foreach($comment as $row)
                                <li>
                                    <div class="comment">
                                        <div class="comment__image">
                                            <img alt="pic" src="{{ asset('frontend/img/avatar-small-1.png') }}" />
                                        </div>
                                        <div class="comment__text">
                                            <h5>{{ $row->username }}</h5>
                                            <span>
                                                <em>{{ $row->new_date }}</em>
                                            </span>
                                            <p>
                                                {{ $row->comment }}
                                            </p>
                                        </div>
                                        <hr>
                                    </div>
                                    <!--end comment-->
                                    <ul>
                                        @foreach($reply as $row_reply)
                                            @if($row_reply->is_reply == $row->id_comment)
                                            <li>
                                                <div class="comment">
                                                    <div class="comment__image">
                                                        <img alt="pic" src="{{ asset('frontend/img/avatar-small-3.png') }}" />
                                                    </div>
                                                    <div class="comment__text">
                                                        <h5>{{ $row_reply->username }}</h5>
                                                        <span>
                                                            <em>{{ $row_reply->new_date }}</em>
                                                        </span>
                                                        <p>
                                                            {{ $row_reply->comment }}
                                                        </p>
                                                    </div>
                                                    <hr>
                                                </div>
                                                <!--end comment-->
                                            </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </li>
                                @endforeach
                            </ul>
                            <form action="{{ url('comment-submit') }}" method="POST" class="comment__form form--square">
                                @csrf
                                <h6>Leave a Comment</h6>
                                <input placeholder="Your Name" type="text" name="data[username]"  required/>
                                <input placeholder="Email Address" type="email" name="data[email]"  required/>
                                <textarea placeholder="Your Comment" name="data[comment]" rows="4" required></textarea>
                                <button type="submit" class="btn btn--primary">Submit Comment</button>
                            </form>
                        </div>
                        <!--end of blog comments-->
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection