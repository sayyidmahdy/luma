@extends('template.frontend.index')
@section('content')

	<section class="height-70 bg--dark imagebg page-title page-title--animate parallax" data-overlay="6">
	    <div class="background-image-holder">
	        <img alt="image" src="{{ asset('frontend/img/hero16.jpg')}}" />
	    </div>
	    <div class="container pos-vertical-center">
	        <div class="row">
	            <!-- <div class="col-sm-10 col-sm-offset-1 text-center">
	                <h3>Let's make something great</h3>
	                <p class="lead">Pillar is a team of passionate designers, developers and artists working in print and digital. We collaborate with clients to establish bold, effective identities.</p>
	            </div> -->
	        </div>
	        <!--end row-->
	    </div>
	    <!--end container-->
	</section>
	<section class="features features-10">
	    <div class="feature bg--white col-md-4 text-center">
	        <i class="icon icon--lg icon-Map-Marker2"></i>
	        <h4>Drop on in</h4>
	        <p>
	            Suite 203, Level 4
	            <br /> 124 Koornang Road
	            <br /> Carnegie, Victoria 3183
	        </p>
	    </div>
	    <div class="feature bg--secondary col-md-4 text-center">
	        <i class="icon icon--lg icon-Phone-2"></i>
	        <h4>Give us a call</h4>
	        <p>
	            Office: +62 812-2221-695
	        </p>
	    </div>
	    <div class="feature bg--dark col-md-4 text-center">
	        <i class="icon icon--lg icon-Computer"></i>
	        <h4>Connect online</h4>
	        <p>
	            Email:
	            <a href="#">hello.lumastory@gmail.com</a>
	            <br /> Instagram:
	            <a href="#">@luma_story</a>
	        </p>
	    </div>
	</section>
	<section>
	    <div class="container">
	        <div class="row">
	            <div class="col-sm-8 col-sm-offset-2">
	                <form action="{{ url('contact-submit') }}" method="post" class="form--square form-email" id="form-contact" data-success="Thanks for your enquiry, we'll be in touch soon" data-error="Please fill all required fields">
	                	@csrf
	                    <h4 class="text-center">Or reach us right here&hellip;</h4>
	                    <div class="input-with-icon col-sm-12">
	                        <i class="icon-MaleFemale"></i>
	                        <input class="validate-required" type="text" name="name" placeholder="Your Name" />
	                    </div>
	                    <div class="input-with-icon col-sm-6">
	                        <i class="icon-Email"></i>
	                        <input class="validate-required validate-email" type="email" name="email" placeholder="Email Address" />
	                    </div>
	                    <div class="input-with-icon col-sm-6">
	                        <i class="icon-Phone-2"></i>
	                        <input type="text" class="validate-required" name="telephone" placeholder="Phone Number" />
	                    </div>
	                    <!-- <div class="input-with-icon col-sm-6">
	                        <i class="icon-Calendar-4"></i>
	                        <input type="text" class="validate-required" name="dates" class="datepicker" placeholder="Date" />
	                    </div> -->
	                    <div class="input-with-icon col-sm-12">
	                        <i class="icon-Building"></i>
	                        <input type="text" class="validate-required" name="venue" placeholder="venue" />
	                    </div>
	                    <div class="col-sm-12">
	                        <textarea class="validate-required" name="message" placeholder="Your Message" rows="8"></textarea>
	                    </div>
	                    <div class="col-sm-12">
	                        <button type="submit" class="btn btn--primary">
	                            Submit Form
	                        </button>
	                    </div>
	                </form>
	            </div>
	        </div>
	        <!--end of row-->
	    </div>
	    <!--end of container-->
	</section>

@endsection
@push('script')
	<script>
		function contact_submit() {
			var url = base_url + '/contact-submit';
			var dataSend = $('#form-contact').serialize();
			$.ajax({
				url: url,
				type: 'post',
				data: dataSend,
				success: function(res){

				}
			})
		}
	</script>
@endpush