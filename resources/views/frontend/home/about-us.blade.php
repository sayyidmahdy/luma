@extends('template.frontend.index')
@section('content')
<section class="height-100 imagebg parallax" data-overlay="3">
    <div class="background-image-holder">
        <img alt="background" src="https://iluminen.com/assets/file/2018/07/APW3318-scaled.jpg" />
    </div>
</section>
<section style="background: #333333 !important">
    <div class="container">
        <div class="row">
            @foreach($data as $row)
                <div class="col-sm-4 col-xs-6">
                    <div class="hover-element member member-2" data-title-position="center,center">
                        <div class="hover-element__initial">
                            <img alt="Pic" src="{{ asset('upload/about/' .$row->image)}}" />
                        </div>
                        <div class="hover-element__reveal" data-overlay="9">
                            <div class="boxed">
                                <h6>{{ $row->position }}</h6>
                                <h5>{{ $row->name }}</h5>
                            </div>
                            <ul class="social-list pos-absolute pos-bottom">
                                <li>
                                    <a href="{{ $row->twitter }}" target="_blank">
                                        <i class="socicon-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ $row->facebook }}" target="_blank">
                                        <i class="socicon-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ $row->instagram }}" target="_blank">
                                        <i class="socicon-instagram"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--end hover element-->
                </div>
            @endforeach
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>
@endsection