@extends('template.frontend.index')
@section('content')
<style type="text/css">
    @media all and (min-width: 1300px) {
        .form-width {
            max-width: 35% !important;
        }
    }
</style>
	
    <img alt="background" src="{{ asset('img/header.png') }}" style="margin: 0 !important" />
	<div class="main-container transition--fade">
        <section class="height-100 cover cover-8">
            <div class="col-md-12 col-sm-12 bg--white text-center">
                <div class="pos-vertical-center">
                    <div class="text-left">
                        <form action="{{ url('do-login') }}" method="post" class="form-width">
                            @csrf
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="input-with-icon">
                                        <label>Username:</label>
                                        <i class="icon icon-Male-2"></i>
                                        <input type="text" name="username" placeholder="Username" required />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="input-with-icon">
                                        <label>Password:</label>
                                        <i class="icon icon-Security-Check"></i>
                                        <input type="password" name="password" placeholder="&bullet;&bullet;&bullet;&bullet;&bullet;&bullet;" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <div class="input-checkbox">
                                        <div class="inner"></div>
                                        <input type="checkbox" />
                                    </div>
                                    <span class="type--fine-print">Keep me logged in</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn--primary">Login</button>
                                </div>
                            </div>
                            <div class="row text-center">
                                <p class="type--fine-print">
                                    Forgot password?
                                    <a href="#">Start password recovery</a>
                                </p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection