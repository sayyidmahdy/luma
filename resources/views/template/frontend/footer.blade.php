<footer class="bg--dark footer-4">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-4">
                <img alt="logo" class="logo" src="{{ asset('frontend/img/luma-logo.png') }}">
                <ul class="footer__navigation">
                    <li>
                        <a href="{{ url('/') }}"><span>Home</span></a>
                    </li>
                    <li>
                        <a href="{{ url('about-us') }}"><span>About Us</span></a>
                    </li>
                    <li>
                        <a href="{{ url('contact-us') }}"><span>Contact</span></a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4 col-sm-8">
            </div>
            <div class="col-md-4 col-md-offset-1 col-sm-12">
                <h6>Subscribe</h6>
                <p>Get monthly updates and free resources.</p>
                <form action="http://mrareco.createsend.com/t/d/s/kieth/" class="form--merge form--no-labels" data-error="Please fill all fields correctly." data-success="Thanks for signing up! Please check your inbox for confirmation email." id="subForm" method="post" name="subForm">
                    <p>
                        <label for="fieldEmail">Email Address</label>
                        <br>
                        <input class="col-md-8 col-sm-6 validate-required validate-email" id="fieldEmail" name="cm-kieth-kieth" required="" type="email">
                    </p>
                    <p>
                        <button type="submit">Go</button>
                    </p>
                </form>
                <h6>Connect with Us</h6>
                <ul class="social-list">
                    <li>
                        <a href="#"><i class="socicon-twitter"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="socicon-facebook"></i></a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/luma_story/" target="_blank"><i class="socicon-instagram"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer__lower">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 text-center-xs">
                    <span class="type--fine-print">© Copyright
                    <span class="update-year">{{ date('Y') }}</span> Luma Story - All Rights Reserved</span>
                </div>
                <div class="col-sm-6 text-right text-center-xs">
                    <a class="inner-link top-link" href="#top"><i class="interface-up-open-big"></i></a>
                </div>
            </div>
        </div>
    </div>
</footer>