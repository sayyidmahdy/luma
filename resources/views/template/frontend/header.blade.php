<nav class="transition--fade">
    <div class="nav-bar nav--absolute nav--transparent" data-fixed-at="200">
        <div class="nav-module logo-module left">
            <a href="{{ url('/') }}">
                <img class="logo logo-dark" alt="logo" src="{{ asset('frontend/img/luma-logo.png') }}" />
                <img class="logo logo-light" alt="logo" src="{{ asset('frontend/img/luma-logo.png') }}" />
            </a>
        </div>
        <div class="nav-module menu-module left">
            <ul class="menu">
                <li>
                    <a href="{{ url('/') }}">Home</a>
                </li>
                <li class="vpf">
                    <a href="{{ url('about-us') }}">about us</a>
                </li>
                <li>
                    <a href="#">
                        Galery
                    </a>
                    <ul class="multi-column">
                        <li>
                            <ul id="menu_galery">
                                
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="vpf">
                    <a href="{{ url('/contact-us') }}">contact us</a>
                </li>

            </ul>
        </div>
    </div>
    <!--end nav bar-->
    <div class="nav-mobile-toggle visible-sm visible-xs">
        <i class="icon-Align-Right icon icon--sm"></i>
    </div>
</nav>