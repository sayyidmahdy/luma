<div class="nav-container">

        <nav>
            <div class="utility-bar bg--white">
                <div class="nav-module left">
                    <span class="type--fine-print">Web Design &amp; Development
		                Melbourne</span>
                </div>
                <div class="nav-module left">
                    <span class="type--fine-print">(03) 9282 7732</span>
                </div>
                <div class="nav-module right">
                    <ul class="social-list">
                        <li>
                            <a href="#"><i class="socicon-twitter"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="socicon-dribbble"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="socicon-facebook"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="socicon-instagram"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="socicon-pinterest"></i></a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="nav-bar bg--dark" data-fixed-at="700">
                <div class="nav-module logo-module left">
                    <a href="{{ url('/') }}"><img alt="logo" class="logo logo-dark" src="{{ asset('frontend/img/luma-logo.png') }}"> <img alt="logo" class="logo logo-light" src="{{ asset('frontend/img/luma-logo.png') }}"></a>
                </div>
                <div class="nav-module menu-module left">
                    <ul class="menu">
                        <li>
                            <a href="{{ url('/') }}">Home</a>
                        </li>
                        <li class="vpf">
                            <a href="#">about us</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                Galery
                            </a>
                            <ul class="multi-column">
                                <li>
                                    <ul id="menu_galery">
                                        
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="vpf">
                            <a href="{{ url('/contact-us') }}">contact us</a>
                        </li>

                    </ul>
                </div>
            </div>
            <div class="nav-mobile-toggle visible-sm visible-xs">
                <i class="icon-Align-JustifyAll icon icon--sm"></i>
            </div>
        </nav>

    </div>