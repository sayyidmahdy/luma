<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Pillar Multipurpose HTML Template</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="{{ asset('frontend/css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ asset('frontend/css/socicon.css') }}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ asset('frontend/css/iconsmind.css') }}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ asset('frontend/css/interface-icons.css') }}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ asset('frontend/css/owl.carousel.css') }}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ asset('frontend/css/lightbox.min.css') }}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ asset('frontend/css/theme.css') }}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ asset('frontend/css/custom.css') }}" rel="stylesheet" type="text/css" media="all" />
        <link href='https://fonts.googleapis.com/css?family=Lora:400,400italic,700%7CMontserrat:400,700' rel='stylesheet' type='text/css'>
        <link rel="shortcut icon" href="{{ asset('img/favicon_luma.png') }}" />
    </head>
    <body class="scroll-assist">
        <a id="top"></a>
        <div class="loader"></div>
        @include('template.frontend.header')
        
        @yield('content')

        @include('template.frontend.footer')
        <script src="{{ asset('frontend/js/jquery-2.1.4.min.js') }}"></script>
        <script src="{{ asset('frontend/js/isotope.min.js') }}"></script>
        <script src="{{ asset('frontend/js/ytplayer.min.js') }}"></script>
        <script src="{{ asset('frontend/js/easypiechart.min.js') }}"></script>
        <script src="{{ asset('frontend/js/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('frontend/js/lightbox.min.js') }}"></script>
        <script src="{{ asset('frontend/js/twitterfetcher.min.js') }}"></script>
        <script src="{{ asset('frontend/js/smooth-scroll.min.js') }}"></script>
        <script src="{{ asset('frontend/js/scrollreveal.min.js') }}"></script>
        <script src="{{ asset('frontend/js/parallax.js') }}"></script>
        <script src="{{ asset('frontend/js/scripts.js') }}"></script>
    </body>
</html>