<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Luma Story</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Luma Story">
    <meta name="author" content="Luma Story">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('frontend/css/socicon.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('frontend/css/iconsmind.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('frontend/css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('frontend/css/interface-icons.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('frontend/css/owl.carousel.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('frontend/css/lightbox.min.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('frontend/css/theme.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('frontend/css/custom.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('css/toastr.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400italic,700|Montserrat:400,700" rel="stylesheet" type="{{ asset('frontend/text/css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
    <link rel="shortcut icon" href="{{ asset('img/favicon_luma.png') }}" />
</head>

<body class="scroll-assist " data-reveal-selectors="section:not(.masonry):not(:first-of-type):not(.parallax)">
	<div class="loader"></div>
    @include('template.frontend.header')

    <div class="main-container">

        @yield('content')

        @include('template.frontend.footer')
    </div>

    <a href="https://api.whatsapp.com/send?phone=628122221695" id="user-online-widget" class="user-online-widget" target="_blank">
        <div class="user-online-widget--icon">
            <!-- <i class="fa fa-whatsapp"></i> -->
            <i class="icon socicon-whatsapp"></i>
            <!-- <img src="https://www.bse.ac/wp-content/uploads/2019/06/whatsapp-social-media-icon-design-template-vector-png_127011.jpg"> -->
        </div>
    </a>

    <script src="{{ asset('frontend/js/jquery-2.1.4.min.js') }}"></script>
    <script src="{{ asset('frontend/js/isotope.min.js') }}"></script>
    <script src="{{ asset('frontend/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('frontend/js/lightbox.min.js') }}"></script>
    <script src="{{ asset('frontend/js/twitterfetcher.min.js') }}"></script>
    <script src="{{ asset('frontend/js/smooth-scroll.min.js') }}"></script>
    <script src="{{ asset('frontend/js/scrollreveal.min.js') }}"></script>
    <script src="{{ asset('frontend/js/parallax.js') }}"></script>
    <script src="{{ asset('frontend/js/scripts.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/toastr.min.js') }}" type="text/javascript"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript">
        var base_url = "{{ url('/') }}";
        $(function(){
            $(".datepicker").datepicker();
            menu_galery();

            @if(Session::has('message'))
              @php
                  $flashmsg = explode('|', Session::get("message"));
                  $type = $flashmsg[0];
                  $msg = $flashmsg[1];
                  if($errors->any())
                    $error_msg = '<br>'.implode('<br>', $errors->all());
                  else
                    $error_msg = '';
              @endphp
              
              var type = '@php echo $type @endphp';
              var msg = '@php echo $msg . $error_msg @endphp';
              toastr.error(msg, '');

          @endif
        });

        function menu_galery(){
            var url = base_url + '/menu-galery';
            $.get(url, function(res){
                $.each(res, function(key, val){
                    var menu_url = base_url + '/' + val.name;
                    var html = '<li>\
                                    <a href="'+menu_url+'">\
                                        '+ val.name +'\
                                    </a>\
                                </li>';

                    $('#menu_galery').append(html);

                });
            });
        }
    </script>
</body>

</html>