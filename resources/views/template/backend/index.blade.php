<!DOCTYPE html>
<html lang="en">
<!-- begin::Head -->

<head>
    <meta charset="utf-8" />
    <title>
        Lumastory | {{ ucfirst($breadcrumb[0]) }}
    </title>
    <meta name="description" content="Lumastory Dashboard">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {
                "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
            },
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Web font -->
    <!--begin::Base Styles -->
    <!--begin::Page Vendors -->
    <link href="{{ asset('backend/vendors/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />
    <!--end::Page Vendors -->
    <link href="{{ asset('backend/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/demo/default/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/app/css/dropify.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" />
    <!--end::Base Styles -->
    <link rel="shortcut icon" href="{{ asset('img/favicon_luma.png') }}" />

    <!-- DataTables -->
    <link href="{{ asset('backend/app/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
</head>
<!-- end::Head -->
<!-- end::Body -->

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
    <!-- begin:: Page -->
    <div class="m-grid m-grid--hor m-grid--root m-page">
        <!-- BEGIN: Header -->
        @include('template.backend.header')
        <!-- END: Header -->
        <!-- begin::Body -->
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
            <!-- BEGIN: Left Aside -->
            <button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
                <i class="la la-close"></i>
            </button>
            @include('template.backend.sidebar')
            <!-- END: Left Aside -->
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
            	<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">
								{{ ucfirst($breadcrumb[0]) }}
							</h3>
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="#" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">
									-
								</li>
								@php $lastElement = end($breadcrumb); @endphp
								@foreach($breadcrumb as $key => $val)
								<li class="m-nav__item">
									<a href="" class="m-nav__link">
										<span class="m-nav__link-text">
											{{ ucfirst($val) }}
										</span>
									</a>
								</li>
								@if($val != $lastElement)
								<li class="m-nav__separator">
									-
								</li>
								@endif
								@endforeach
							</ul>
						</div>
					</div>
				</div>
            	@yield('content')
            </div>
        </div>
        <!-- end:: Body -->
    </div>
    <!-- end:: Page -->
    <!-- begin::Scroll Top -->
    <div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
        <i class="la la-arrow-up"></i>
    </div>
    <!-- end::Scroll Top -->
    <!--begin::Base Scripts -->
    <script src="{{ asset('backend/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('backend/demo/default/base/scripts.bundle.js') }}" type="text/javascript"></script>
    <!--end::Base Scripts -->
    <!--begin::Page Vendors -->
    <script src="{{ asset('backend/vendors/custom/fullcalendar/fullcalendar.bundle.js') }}" type="text/javascript"></script>
    <!--end::Page Vendors -->
    <!--begin::Page Snippets -->
    <script src="{{ asset('backend/app/js/dashboard.js') }}" type="text/javascript"></script>
    <!--end::Page Snippets -->

    <script src="{{ asset('backend/demo/default/custom/components/forms/widgets/form-repeater.js') }}" type="text/javascript"></script>
    <script src="{{ asset('backend/demo/default/custom/components/forms/widgets/dropzone.js') }}" type="text/javascript"></script>
    <script src="{{ asset('backend/app/js/dropify.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/custom.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/backend_validator.js') }}" type="text/javascript"></script>
    <script src="{{ asset('backend/app/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('backend/app/js/dataTables.bootstrap4.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('backend/app/js/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('backend/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js') }}" type="text/javascript"></script>
    <!-- <script src="{{ asset('backend/demo/default/custom/components/datatables/base/data-local.js') }}" type="text/javascript"></script> -->
    <script src="https://www.amcharts.com/lib/4/core.js"></script>
    <script src="https://www.amcharts.com/lib/4/charts.js"></script>
    <script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

    <script type="text/javascript">
        var base_url = "{{ url('/') }}";
    	$(function(){
    		exec_dropify();

            @if(Session::has('message'))
              @php
                  $flashmsg = explode('|', Session::get("message"));
                  $type = $flashmsg[0];
                  $msg = $flashmsg[1];
                  if($errors->any())
                    $error_msg = '<br>'.implode('<br>', $errors->all());
                  else
                    $error_msg = '';
              @endphp
              
              var type = '@php echo $type @endphp';
              var msg = '@php echo $msg . $error_msg @endphp';
              setTimeout(function() { showFlashAlert(type, msg); }, 1000);

          @endif
    	});

    	function exec_dropify(){
    		$('.dropify').dropify({
	            messages: {
	                'default': 'Drag and drop a file here or click',
	                'replace': 'Drag and drop or click to replace',
	                'remove': 'Remove',
	                'error': 'Ooops, something wrong appended.',
	            },
	            error: {
	                'fileSize': 'The file size is too big (3MB max).',
	                'imageFormat': 'The image format is not allowed (jpg, jpeg, png) only.'
	            }
	        });
    	}
    </script>

    @stack('script')

    <div id="flash_msg">
      <div class="alert alert-dismissible alert-success" role="alert">
        <span class="alert-inner--text">
          <strong id="alert-title">Success</strong><br>
          <span id="alert-message">Data Berhasil Disimpan</span>
        </span>
        <button type="button" class="close" aria-label="Close" onclick="closeFlashAlert()">
          <span aria-hidden="true">×</span>
        </button>
      </div>
    </div>
</body>
<!-- end::Body -->


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135611078-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-135611078-1');
</script>

</html>